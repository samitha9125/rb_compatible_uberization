
import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Colors, Images } from '../config';
import { Text, Label } from './Typography';
import * as Progress from 'react-native-progress';

class Button extends Component {

	render() {
		const textStyle = StyleSheet.flatten(this.props.childrenStyle);
		const body =
			this.props.showLoader ?
				<Progress.Circle
					size={30}
					color={Colors.COLOR_WHITE}
					borderWidth={3}
					indeterminate={true} /> :
				this.props.labelSize ?
					<Label weight={this.props.weight} style={textStyle}>{this.props.children}</Label> :
					<Text weight={this.props.weight} style={textStyle}>{this.props.children}</Text>;

		return (
			<TouchableOpacity
				disabled={this.props.disabled}
				style={[
					Styles.buttonStyle,
					this.props.style,
					this.props.disabled ? Styles.buttonDisabled : {},
					this.props.alignBottom ? Styles.alignButtonBottom : {}
				]}
				onPress={this.props.showLoader ? null : this.props.pressed}>
				{body}
			</TouchableOpacity>
		);
	}
}

const Styles = StyleSheet.create({
	buttonStyle: {
		height: 50,
		width: '100%',
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		backgroundColor: Colors.COLOR_PRIMARY
	},
	buttonDisabled: {
		backgroundColor: Colors.COLOR_GRAY_LIGHT
	},
	alignButtonBottom: {
		bottom: 0,
		left: 0,
		position: 'absolute'
	},
	textMargin: {
		marginLeft: 5
	},
	loader: {
		height: 10,
		width: 10
	}
});


export default Button;
