import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Images } from '../../config/Images';
import { Constants, Colors } from '../../config';
import { Label } from '../../components/Typography';
import { getLocalizedString } from '../../utils/Localizer';
import * as appActions from '../../redux/actions/AppActions'
import { verticalScale } from '../../helpers/scaling';
class TabComponent extends Component {

    constructor(props) {
        super(props);
        this.localization = getLocalizedString('en', Constants.TabNavigator);
    }

    assetsSwitcher = (type, focused) => {
        switch (type) {
            case Constants.screenTypes.MyJobs:
                return focused ? Images.icons.tab_job_enabled : Images.icons.tab_job_disabled;
            case Constants.screenTypes.Completed:
                return focused ? Images.icons.tab_completed_enabled : Images.icons.tab_completed_disabled;
            case Constants.screenTypes.Upcoming:
                return focused ? Images.icons.tab_upcoming_enabled : Images.icons.tab_upcoming_disabled;
            case Constants.screenTypes.Profile:
                return focused ? Images.icons.tab_profile_enabled : Images.icons.tab_profile_disabled;
        }
    }

    render() {
        const imageToRender = <Image
            style={styles.image}
            source={this.assetsSwitcher(this.props.type, this.props.focused)} />;

        return (
            <View style={styles.container}>
                {imageToRender}
                <Label
                    weight={this.props.focused ? 'bold' : 'light'}
                    style={{ color: this.props.focused ? Colors.COLOR_BLACK : Colors.COLOR_GRAY }}>
                    {this.localization[this.props.type]}
                </Label>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: verticalScale(25),
        marginBottom: verticalScale(17),
        paddingBottom: verticalScale(23),
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        marginTop: 15,
        marginBottom: 2,
        width: 24,
        height: 24
    }
});

export default TabComponent
