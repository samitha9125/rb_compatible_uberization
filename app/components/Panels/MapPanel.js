import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, Modal, Text, Animated, TouchableOpacity } from 'react-native';
import Interactable from 'react-native-interactable';
import { Colors } from '../../config/Colors'

export default class MapPanel extends Component {
    constructor(props) {
        super(props);
        this._deltaY = new Animated.Value(Screen.height - 100);
        this.state = {
            headerHeight: 0
        };
        this.snapPointsArray = [];
        this.animatedViewRef = React.createRef();
    }

    onLayoutHeader = (event) => {
        const height = event.nativeEvent.layout.height;
        let arr = [{ y: Screen.height - height }, { y: Screen.height }];
        this.snapPointsArray = arr;

        this.setState({ headerHeight: height }, () => {
            this.animatedViewRef.snapTo({ index: 0 });
        });

    }

    onDrawerSnap = (event) => {
        if (event.nativeEvent.index === 1) {
            this.props.hideModal();
        }
    }

    render() {
        return (
            <Modal
                visible={this.props.show}
                transparent={true}
                animationType={'slide'}
                onShow={this.onShow}
                onRequestClose={() => { }} >
                <View style={styles.container}>
                    <View style={styles.panelContainer} pointerEvents='box-none'>
                        <Interactable.View
                            ref={ref => this.animatedViewRef = ref}
                            verticalOnly={true}
                            onSnap={this.onDrawerSnap}
                            snapPoints={this.snapPointsArray}
                            boundaries={{ top: -300 }}
                            initialPosition={{ y: Screen.height - 300 }}
                            animatedValueY={this._deltaY}>
                            <View style={styles.panel}>
                                <View style={styles.panelHeaderView} onLayout={this.onLayoutHeader}>
                                    <View style={styles.panelHandle} />
                                    {this.props.header()}
                                </View>
                            </View>
                        </Interactable.View>
                    </View>
                </View>
            </Modal>
        );
    }
}

const Screen = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    panelContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    panel: {
        height: Screen.height + 300,
        padding: 20,
        backgroundColor: Colors.COLOR_WHITE,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 5,
        shadowOpacity: 0.4
    },
    panelHeaderView: {
        width: '100%',
        alignItems: 'center',
        paddingBottom: 40
    },
    panelHandle: {
        width: 40,
        height: 4,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10
    },
    panelHeaderWrapper: {
        width: '100%',
        alignItems: 'flex-start'
    },
    panelHeader: {
        fontSize: 24
    }
});
