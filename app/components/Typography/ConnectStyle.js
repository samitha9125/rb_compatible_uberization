import React from 'react';
import { StyleSheet } from 'react-native';
import Styles from './Styles';

const translateProps = (props, componentDisplayName) => {
    const { weight, align, style } = props;
    let changeStyleOnFly = {
        ...style,
        fontWeight: weight ? weight === 'light' ? '300' : weight : 'normal',
        fontFamily: weight ? weight === 'light' ? 'Roboto-Light' : 'Roboto-Bold' : 'Roboto-Regular',
        textAlign: align ? align : 'left'
    };

    switch (componentDisplayName) {
        case 'heading':
            return { ...props, style: StyleSheet.flatten([Styles.heading, changeStyleOnFly]) };
        case 'title':
            return { ...props, style: StyleSheet.flatten([Styles.title, changeStyleOnFly]) };
        case 'subtitle':
            return { ...props, style: StyleSheet.flatten([Styles.subtitle, changeStyleOnFly]) };
        case 'caption':
            return { ...props, style: StyleSheet.flatten([Styles.caption, changeStyleOnFly]) };
        case 'text':
            return { ...props, style: StyleSheet.flatten([Styles.text, changeStyleOnFly]) };
        case 'link':
            return { ...props, style: StyleSheet.flatten([Styles.link, changeStyleOnFly]) };
        case 'largerHeading':
            return { ...props, style: StyleSheet.flatten([Styles.largerHeading, changeStyleOnFly]) };
        case 'largeMediumHeading':
            return { ...props, style: StyleSheet.flatten([Styles.largeMediumHeading, changeStyleOnFly]) };
        case 'label':
            return { ...props, style: StyleSheet.flatten([Styles.textSmall, changeStyleOnFly]) };
        default:
            return props;
    }
};


export default (componentStyleName, componentStyle = {}, mapPropsToStyleNames, options = {}) => {

    return function wrapWithStyledComponent(WrappedComponent) {
        return function wrappedRender(args) {
            return <WrappedComponent {...translateProps(args, componentStyleName)} />;
        };
    };
};

