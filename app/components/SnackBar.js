import React, { Component } from 'react';
import { View, Modal, Animated, StyleSheet } from 'react-native';
import { Text } from './Typography';
import { Colors } from '../config';
import { connect } from 'react-redux';
import normalize from '../helpers/normalizeText';

const information = {
    online: {
        id: 1,
        message: 'Network connection is established',
        status: 'CONNECTED'
    },
    offline: {
        id: 2,
        message: 'No internet connection',
        status: 'DISCONNECTED'
    }
};

class SnackBar extends Component {

    constructor() {
        super();
        this.state = {
            show: false,
            info: information.online
        }
        this.animatedValue = new Animated.Value(120);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isOnline !== this.props.isOnline) {

            if (this.props.isOnline) { // Connection established.
                this.setState({ info: information.online });
                setTimeout(() => {
                    Animated.timing
                        (this.animatedValue,
                            {
                                toValue: 120,
                                duration: 350
                            }
                        ).start(() => this.setState({ show: false }));
                }, 1000);
            }
            else { // Connection lost
                this.setState({ info: information.offline }, () => {
                    Animated.timing(this.animatedValue,
                        {
                            toValue: 0,
                            duration: 350
                        }
                    ).start();
                    this.setState({ show: true });
                });
            }
        }
    }

    render() {
        const { info } = this.state;
        return (
            <Modal
                visible={this.state.show}
                transparent={true}
                animationType={'fade'}
                onRequestClose={() => { }} >
                <View style={styles.modalView}>
                    <Animated.View
                        useNativeDriver={true}
                        style={[styles.snackbar, { transform: [{ translateY: this.animatedValue }] }]} >
                        <Text
                            weight='light'
                            style={StyleSheet.flatten(styles.message)}>
                            {info.message}</Text>
                        <Text
                            weight='light'
                            style={StyleSheet.flatten(info.id === 1 ? styles.statusOnline : styles.statusOffline)}>
                            {info.status}</Text>
                    </Animated.View>
                    <View style={styles.bottomNavigator} />
                </View>
            </Modal>
        );
    }
}

SnackBar.propTypes = {

};

const styles = StyleSheet.create({
    modalView: {
        flex: 1,
        backgroundColor: Colors.COLOR_TRANSPARENT,
        justifyContent: 'flex-end'
    },
    bottomNavigator: {
        height: 59.5,
        width: '100%',
        backgroundColor: Colors.COLOR_TRANSPARENT,
        zIndex: 1
    },
    snackbar: {
        zIndex: -1,
        backgroundColor: Colors.COLOR_SNACK_BAR,
        padding: 10,
        paddingTop: 16,
        paddingBottom: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    message: {
        flex: 1,
        color: Colors.COLOR_WHITE,
        fontSize: normalize(13)
    },
    statusOnline: {
        color: Colors.COLOR_GREEN,
        fontSize: normalize(15)
    },
    statusOffline: {
        color: Colors.COLOR_ERROR,
        fontSize: normalize(15)
    }
});

const mapStateToProps = state => {
    return {
        isOnline: state.appReducer.isAppOnline
    };
};

export default connect(mapStateToProps, null)(SnackBar);

