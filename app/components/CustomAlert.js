import React, { Component } from 'react';
import { View, Modal, TouchableOpacity, Image, ScrollView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import * as appActions from '../redux/actions/AppActions';
import * as jobActions from '../redux/actions/JobActions';
import { Images, Colors, Constants } from '../config';
import { Caption, Text } from './Typography';
import { scale } from '../helpers/scaling';

class CustomAlert extends Component {

	hideAlert = () => {
		this.props.hideAlert();
	}

	onPressActiveButton = () => {
		switch (this.props.positiveButtonActionType) {
			case Constants.alertActionTypes.PROCEED_TO_ASSIGN_JOB:
				this.props.assignJob(Constants.jobStatus.ASSIGNED);
				this.hideAlert();
			default:
				this.hideAlert();
				break;
		}

	}

	onPressNegativeButton = () => {
		switch (this.props.negativeButtonActionType) {
			default:
				this.hideAlert();
				break;
		}
	}

	selectMessageIcon = (type) => {
		switch (type) {
			case Constants.alertType.WARNING:
				return <Image style={styles.icon} source={Images.icons.ic_warning} />;
			case Constants.alertType.ERROR:
				return <Image style={styles.icon} source={Images.icons.ic_error} />;
			default:
				break;
		}
	}

	render() {
		const errorType = this.props.type === Constants.alertType.ERROR;
		const negativeButton = this.props.negativeButton ?
			<TouchableOpacity
				style={styles.buttonStyle}
				onPress={this.onPressNegativeButton}
				activeOpacity={0.7}>
				<Text weight='bold'
					style={StyleSheet.flatten(styles.negativeButtonText)}>
					{this.props.negativeButtonText}
				</Text>
			</TouchableOpacity>
			: null;

		const positiveButton = this.props.positiveButton ?
			<TouchableOpacity
				style={styles.buttonStylePositive}
				onPress={this.onPressActiveButton}
				activeOpacity={0.7}>
				<Text weight='bold'
					style={StyleSheet.flatten(errorType ? styles.positiveButtonTextError : styles.positiveButtonText)}>
					{this.props.positiveButtonText}
				</Text>
			</TouchableOpacity>
			: null;

		const modalContent = errorType ? styles.modalErrorContent : styles.modalContent;

		return (
			<Modal
				visible={this.props.show}
				transparent={true}
				animationType={'fade'}
				onRequestClose={this.hideAlert} >
				<View style={modalContent}>
					<View style={styles.Alert_Main_View}>
						<View style={styles.header}>
							{this.selectMessageIcon(this.props.type)}
							<Caption weight='bold' style={StyleSheet.flatten(styles.title)}>{this.props.title}</Caption>
						</View>
						<ScrollView contentContainerStyle={styles.scroller}>
							<Text
								weight={errorType ? 'bold' : 'light'}
								style={StyleSheet.flatten(errorType ? styles.erroBody : styles.body)}>
								{this.props.message}
							</Text>
						</ScrollView>
						<View style={styles.footer}>
							{negativeButton}
							{positiveButton}
						</View>
					</View>
				</View>
			</Modal>
		);
	}
}

CustomAlert.propTypes = {
};

const mapStateToProps = state => {
	return {
		show: state.appReducer.show,
		type: state.appReducer.type,
		title: state.appReducer.title,
		message: state.appReducer.message,
		positiveButton: state.appReducer.positiveButton,
		positiveButtonActionType: state.appReducer.positiveButtonActionType,
		positiveButtonText: state.appReducer.positiveButtonText,
		negativeButton: state.appReducer.negativeButton,
		negativeButtonText: state.appReducer.negativeButtonText,
		negativeButtonActionType: state.appReducer.negativeButtonActionType,
	};
};

function mapDispatchToProps(dispatch) {
	return {
		hideAlert: () => dispatch(appActions.hideAlert()),
		assignJob: (payload) => dispatch(jobActions.assignJobAction(payload)),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomAlert);

const styles = {
	modalContent: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Colors.COLOR_BLACK_LIGHT
	},
	modalErrorContent: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Colors.COLOR_BLACK
	},
	Alert_Main_View: {
		justifyContent: 'space-around',
		backgroundColor: Colors.COLOR_WHITE,
		width: '75%',
		padding: 15,
		paddingLeft: scale(20),
		paddingRight: scale(20),
		borderWidth: 1,
		borderRadius: 4,
		borderColor: Colors.COLOR_WHITE,
	},
	header: {
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 20
	},
	icon: {
		height: 40,
		width: 40,
	},
	footer: {
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginTop: 29
	},
	positiveButtonText: {
		color: Colors.COLOR_YELLOW
	},
	positiveButtonTextError: {
		color: Colors.COLOR_ERROR
	},
	negativeButtonText: {
		color: Colors.COLOR_BLACK
	},
	buttonStylePositive: {
		marginLeft: 40
	},
	body: {
		lineHeight: 24,
		color: Colors.COLOR_BLACK_LIGHT
	},
	erroBody: {
		lineHeight: 24,
		color: Colors.COLOR_BLACK
	}
};

