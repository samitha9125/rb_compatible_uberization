import { pulseApi } from '../api/methods/App'
import { call, select } from 'redux-saga/effects'
import { pmsID } from '../redux/reducers/Selectors';

export function* heartBeatSaga() {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    try {
        console.log('Sending pulses ... ');
        const data = {
            pmsId: ID
        };
        response = yield call(pulseApi, accessToken, data);
        console.log(response);
    } catch (error) {
        console.log(error);
    }
}