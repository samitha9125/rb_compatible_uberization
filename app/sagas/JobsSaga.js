import {
    gerUserStatus,
    getUpcomingJobs,
    getMarkerDetails,
    assignJob,
    getAssignedJobs,
    getWoPathDetails,
    changeUserStatus,
    updateAssignedJob,
    callLog
} from '../api/methods/Job';
import { call, put, select } from 'redux-saga/effects';
import * as types from '../redux/actions/types';
import { marker, pmsID } from '../redux/reducers/Selectors';
import { Constants } from '../config';
import NavigationService from '../navigation/NavigationService';

export function* getUserStatusSaga() {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);
    yield put({ type: types.FETCHING_USER_ONLINE_STATUS, payload: true });
    try {
        console.log('Requesting user status ... ');
        const data = {
            pmsId: ID
        };
        response = yield call(gerUserStatus, accessToken, data);
        console.log(response);
    } catch (error) {
        yield put({ type: types.FETCHING_USER_ONLINE_STATUS, payload: false });
        yield call(handleError, error);
    }

    if (response.data) {
        yield put({ type: types.STORE_USER_ONLINE_STATUS, payload: response.data.statusDescription });
        yield put({ type: types.FETCHING_USER_ONLINE_STATUS, payload: false });
        
        if (response.data.statusDescription === Constants.userStatus.ONLINE) { // If user is online
            yield call(getAssignedJobsSaga);
            yield put({ type: types.STORE_TEAM_LOCATION, payload: response.data.driverLocation });
            if (response.data.recentWoDetails) { // if not null, user is on a job.
                yield put({
                    type: types.STORE_ASSIGNED_WO_DETAILS,
                    payload: {
                        encryptedPath: response.data.recentWoDetails.encryptedPath,
                        woId: response.data.recentWoDetails.woId
                    }
                });
                yield put({ type: types.UPDATE_USER_JOB_PERFORMING_STATUS, payload: true });
            }
        }
    }

}

export function* userStatusChangeSaga(action) {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);
    yield put({ type: types.FETCHING_USER_ONLINE_STATUS, payload: true });

    try {
        const data = {
            pmsId: ID,
            status: action.payload
        };
        console.log('changing user status ... ',data);
        response = yield call(changeUserStatus, accessToken, data);
        console.log(response);
    } catch (error) {
        yield put({ type: types.FETCHING_USER_ONLINE_STATUS, payload: false });
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        yield put({ type: types.STORE_USER_ONLINE_STATUS, payload: Constants.userStatus.ONLINE });
        yield put({ type: types.FETCHING_USER_ONLINE_STATUS, payload: false });
        yield call(getAssignedJobsSaga);
        yield put({ type: types.STORE_TEAM_LOCATION, payload: response.data.driverLocation });

        /**
         * Need to use the same function for making User offline. Improve this. 
         */

        if (response.data.recentWoDetails) { // if not null, user is on a job.
            yield put({
                type: types.STORE_ASSIGNED_WO_DETAILS,
                payload: {
                    encryptedPath: response.data.recentWoDetails.encryptedPath,
                    woId: response.data.recentWoDetails.woId
                }
            });
            yield put({ type: types.UPDATE_USER_JOB_PERFORMING_STATUS, payload: true });
        }
    }

}

export function* getUpcomingJobsSaga() {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
        woStatusCode: 'Pooled'
    };

    try {
        console.log('get upcming jobs ... ');
        response = yield call(getUpcomingJobs, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        yield put({ type: types.STORE_UPCOMING_JOBS, payload: response.data.woRecords });
    }

}

export function* getMarkerDetailsSaga(action) {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
        woId: action.payload
    };
    try {
        yield put({ type: types.FETCHING_MARKER_DETAILS });
        console.log('requesting marker details ... ', data);
        response = yield call(getMarkerDetails, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        yield put({ type: types.STORE_MARKER_DETAILS, payload: response.data.woDetails });
    }

}

export function* assignJobSaga(action) {
    let response = {};
    let accessToken = '';
    const markerObj = yield select(marker);
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
        womsWoId: markerObj.woId
    };

    try {
        yield put({ type: types.FETCHING_MARKER_DETAILS });
        console.log('assigning job ... ', data);
        response = yield call(assignJob, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        if (action.payload === Constants.jobStatus.ASSIGNED)
            yield call(getUpcomingJobsSaga);
    }

}

export function* updateAssignedJobSaga(action) {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
        woId: action.payload.workOrder.woId,
        woStatusCode: action.payload.status
    };

    try {
        yield put({ type: types.FETCHING_MARKER_DETAILS });
        console.log('updating an assigned job ... ', data);
        response = yield call(updateAssignedJob, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) { // API call is success.
        if (action.payload.workOrder.woStatusId === 1) {
            yield put({ type: types.UPDATE_USER_JOB_PERFORMING_STATUS, payload: true });
            yield call(getWoPathSaga, { payload: action.payload.workOrder.woId })
        }
        else {
            // Should handle proceed button and arrived
        }
    }

}

export function* getAssignedJobsSaga(action) {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
    };

    try {
        yield put({ type: types.FETCHING_MARKER_DETAILS });
        console.log('get already assigned jobs ... ', data);
        response = yield call(getAssignedJobs, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        console.log('getAssignedJobsSaga response : ', response.data);
        yield put({ type: types.STORE_ASSIGNED_JOBS, payload: response.data.woRecords });
    }

}

export function* getWoPathSaga(action) {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
        woId: action.payload
    };

    try {
        yield put({ type: types.STORE_ASSIGNED_WO_ID, payload: action.payload })
        console.log('getting assigned job path and data... ', data);
        response = yield call(getWoPathDetails, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        console.log('getWoPathDetails response : ', response.data);
        yield put({ type: types.STORE_ASSIGNED_WO_DETAILS, payload: response.data.woDetails });
        NavigationService.Navigate('Map');
    }

}

export function* callLogSaga(action) {
    let response = {};
    let accessToken = '';
    const ID = yield select(pmsID);

    const data = {
        pmsId: ID,
        ...action.payload
    };

    try {
        console.log('Logging call data... ', data);
        response = yield call(callLog, accessToken, data);
        console.log(response);
    } catch (error) {
        yield call(handleError, error);
    }

    if (response.data && response.data.status === 1) {
        console.log('callLog response : ', response.data);
    }

}

export function* handleError(error) {
    console.log(error)
    const payload = {
        show: true,
        type: Constants.alertType.ERROR,
        title: '',
        message: 'Something went wrong. Please try again later.',
        positiveButton: true,
        positiveButtonActionType: Constants.alertActionTypes.SOMETHING_WENT_WRONG,
        positiveButtonText: 'OK',
        negativeButton: false,
        negativeButtonText: '',
        negativeButtonActionType: '',
        additionalPayload: {}
    };
    yield put({ type: types.SHOW_ALERT, payload });
}

