import { takeEvery } from 'redux-saga/effects';
import * as types from '../redux/actions/types';
import {
    heartBeatSaga,
} from './AppSaga';
import {
    getUserStatusSaga,
    getUpcomingJobsSaga,
    getMarkerDetailsSaga,
    assignJobSaga,
    getAssignedJobsSaga,
    getWoPathSaga,
    userStatusChangeSaga,
    updateAssignedJobSaga,
    callLogSaga
} from './JobsSaga';

export default function* watch() {
    yield takeEvery(types.HEART_PULSE, heartBeatSaga);
    yield takeEvery(types.USER_ONLINE_STATUS, getUserStatusSaga);
    yield takeEvery(types.GET_UPCOMING_JOBS, getUpcomingJobsSaga);
    yield takeEvery(types.GET_MARKER_DETAILS, getMarkerDetailsSaga);
    yield takeEvery(types.ASSIGN_A_JOB, assignJobSaga);
    yield takeEvery(types.GET_ASSIGNED_JOBS, getAssignedJobsSaga);
    yield takeEvery(types.GET_WO_PATH_DETAILS, getWoPathSaga);
    yield takeEvery(types.CHANGE_USER_ONLINE_STATUS_BACKEND, userStatusChangeSaga);
    yield takeEvery(types.UPDATE_ASSIGNED_JOB, updateAssignedJobSaga);
    yield takeEvery(types.PUT_CALL_LOG, callLogSaga);
}
