import React, { Component } from 'react';
import StackNavigator from './navigation/StackNavigator'
import configureStore from './utils/configureStore';
import { Provider } from 'react-redux';

const { store } = configureStore();
class Entrypoint extends Component {

    constructor(props) {
        super(props);
        console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
    }

    onPressHome = () => {
        this.props.onPressHome();
    }

    render() {
        return (
            <Provider store={store}>
                <StackNavigator
                    screenProps={{ onPressHome: this.onPressHome }} />
            </Provider>
        );
    }
}

export default Entrypoint;