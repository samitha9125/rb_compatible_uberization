import axios from 'axios';
import { ApiEndpoint } from './ApiConstants';

import * as mocks from '../api/mock/';

//API mocks
// mocks.appMocked(axios, 100);
// mocks.jobMocked(axios, 100);

export default function api(URL, method, token, params, data) {

    let options;
    options = {
        headers: {
            ...(token ? {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                Authorization: `bearer ${token}`
            } : {})
        },
        ...(params ? { params: params } : {}),
        ...(data ? { data: data } : {}),

    };

    return axios.create({
        baseURL: ApiEndpoint.BASE_URL,
        method: method,
        timeout: 20000,
        url: URL,
        ...options
    });
}