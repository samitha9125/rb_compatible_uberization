export const Method = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
};

export const ApiEndpoint = {
    // Base URLs
    BASE_URL: 'http://uberization-dev-lb-197075116.ap-southeast-1.elb.amazonaws.com', // Dev

    //Auth
    APP_PULSE: '/api/driver-pulse/active',
    CUSTOMER_STATUS: '/api/driver-profile/getDriverOnlineStatus',
    PUT_CUSTOMER_STATUS: '/api/driver-profile/updateDriverOnlineStatus',

    // Job
    GET_UPCOMING_JOBS: '/api/wo-management/pooled',
    GET_MARKER_DETAILS: '/api/wo-management/pooled/details',
    ASSIGN_JOB: '/api/wo-management/pooled/assign',
    GET_ASSIGNED_JOBS: '/api/wo-management/assigned',
    GET_WO_PATH: '/api',
    UPDATE_ASSIGNED_JOB: '/api/wo-management/updateWoStatus',
    CALL_LOG: '/api/wo-management/callLog',

};

export const ApiResponse = {

};

