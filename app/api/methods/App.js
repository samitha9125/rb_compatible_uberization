import { Method, ApiEndpoint } from '../ApiConstants';
import Api from '../index';

export function pulseApi(accessToken, data) {
    return Api(ApiEndpoint.APP_PULSE, Method.PUT, accessToken, null, data)({});
}