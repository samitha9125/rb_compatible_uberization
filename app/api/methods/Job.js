import { Method, ApiEndpoint } from '../ApiConstants';
import Api from '../index';

export function gerUserStatus(accessToken,data) {
    return Api(ApiEndpoint.CUSTOMER_STATUS, Method.POST, accessToken, null, data)({});
}

export function changeUserStatus(accessToken,data) {
    return Api(ApiEndpoint.PUT_CUSTOMER_STATUS, Method.POST, accessToken, null, data)({});
}

export function getUpcomingJobs(accessToken,data) {
    return Api(ApiEndpoint.GET_UPCOMING_JOBS, Method.POST, accessToken, null, data)({});
}

export function getMarkerDetails(accessToken,data) {
    return Api(ApiEndpoint.GET_MARKER_DETAILS, Method.POST, accessToken, null, data)({});
}

export function assignJob(accessToken,data) {
    return Api(ApiEndpoint.ASSIGN_JOB, Method.POST, accessToken, null, data)({});
}

export function getAssignedJobs(accessToken,data) {
    return Api(ApiEndpoint.GET_ASSIGNED_JOBS, Method.POST, accessToken, null, data)({});
}

export function getWoPathDetails(accessToken,data) {
    return Api(ApiEndpoint.GET_WO_PATH, Method.POST, accessToken, null, data)({});
}

export function updateAssignedJob(accessToken,data) {
    return Api(ApiEndpoint.UPDATE_ASSIGNED_JOB, Method.PUT, accessToken, null, data)({});
}

export function callLog(accessToken,data) {
    return Api(ApiEndpoint.CALL_LOG, Method.POST, accessToken, null, data)({});
}