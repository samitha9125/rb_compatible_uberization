import mockApi from './MockAdapter';
import { ApiEndpoint } from '../ApiConstants';

export function appMocked(Api, delay) {
    let mock = mockApi(Api, delay);
    
    mock.onPost(ApiEndpoint.appPulse)
        .reply(200, {
            STATUS: 'SUCCESS'
        });
}