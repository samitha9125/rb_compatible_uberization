import mockApi from './MockAdapter';
import { ApiEndpoint } from '../ApiConstants';

export function jobMocked(Api, delay) {
    let mock = mockApi(Api, delay);

    mock.onPost(ApiEndpoint.CUSTOMER_STATUS)
        .reply(200, {
            status: 1,
            statusDescription: 'Online',
            recentWoDetails: null,
            // recentWoDetails: {
            //     woId: 0,
            //     woStatusId: 2,
            //     woStatusCode: 'Started',
            //     woType:'Delivery',
            //     productCategoryId:1,
            //     encryptedPath: 'yybi@ctkfNMs@j@MzA[lEaAbAU`@Ik@cDk@yC_@sCqAuHqE`A{D~@cKzB}@H]?sCSwAIg@Ig@e@a@e@kDrAy@\\}HvCeDlAwEdB}D|AeAl@A`@C|AEbDCxCAvDEtDK~@DDBNGNSHI?ICM@UBO?kAPSLOT?FLTBLDVH`@'
            // },
            driverLocation: {
                latitude: 6.912815,
                longitude: 79.850677
            }
        });
    mock.onPost(ApiEndpoint.PUT_CUSTOMER_STATUS)
        .reply(200, {
            status: 1,
            statusDescription: "Success",
            recentWoDetails: {
                woId: 0,
                woStatusId: 2,
                woStatusCode: 'Started',
                woType: 'Delivery',
                productCategoryId: 1,
                encryptedPath: 'yybi@ctkfNMs@j@MzA[lEaAbAU`@Ik@cDk@yC_@sCqAuHqE`A{D~@cKzB}@H]?sCSwAIg@Ig@e@a@e@kDrAy@\\}HvCeDlAwEdB}D|AeAl@A`@C|AEbDCxCAvDEtDK~@DDBNGNSHI?ICM@UBO?kAPSLOT?FLTBLDVH`@'
            },
            driverLocation: {
                latitude: 6.912815,
                longitude: 79.850677
            }
        });
    mock.onPost(ApiEndpoint.GET_UPCOMING_JOBS)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS',
            woCount: 1,
            woRecords: [{
                woId: 1,
                productCategoryId: 1,
                cxLocation: {
                    latitude: 6.926799,
                    longitude: 79.863879
                }
            }]
        });
    mock.onPost(ApiEndpoint.GET_MARKER_DETAILS)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS',
            woDetails: {
                woId: 1,
                woStatusId: 2,
                productType: 'prepaidSim',
                woType: 'delivery',
                paymentType: 'Cash On Delivery',
                productCategoryId: 1,
                woStatusCode: 'Started',
                amount: 242321.65,
                cxDetails: {
                    cxName: 'Uber',
                    cxLocation: {
                        latitude: 423424,
                        longitude: 42342
                    },
                    cxAddress: 'dakjhdaskdjhaldjkahd'
                },
                cxContacNumbers: ['42412443', '42412443'],
                navigation: {
                    distance: 23123,//meters
                    duration: 23,//min
                    encryptedPath: '2642i424bdjfbkdjfqf',
                    decryptedPath: []
                }
            }
        });
    mock.onPost(ApiEndpoint.ASSIGN_JOB)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS'
        });
    mock.onPost(ApiEndpoint.GET_ASSIGNED_JOBS)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS',
            woRecords: [{
                amount: 0,
                cxContactNumbers: [
                    '0711765675', '0765436274'
                ],
                cxDetails: {
                    cxAddress: 'No 374, Fonseka mawatha, sea avaenue, Colombo 04.',
                    cxLocation: {
                        latitude: '',
                        longitude: ''
                    },
                    cxName: 'D.S. Perera'
                },
                cxRequestedTime: '2019 - 05 - 26T17: 53: 08.840Z',
                navigation: {
                    decryptedPath: '',
                    distance: 0,
                    duration: 0,
                    encryptedPath: 'yybi@ctkfNMs@j@MzA[lEaAbAU`@Ik@cDk@yC_@sCqAuHqE`A{D~@cKzB}@H]?sCSwAIg@Ig@e@a@e@kDrAy@\\}HvCeDlAwEdB}D|AeAl@A`@C|AEbDCxCAvDEtDK~@DDBNGNSHI?ICM@UBO?kAPSLOT?FLTBLDVH`@'
                },
                paymentType: 'Card Payment',
                productCategoryId: 1,
                productType: 'TV',
                scheduledTime: '2019 - 05 - 26T17: 53: 08.840Z',
                woId: 0,
                woStatusCode: 'ASSIGNED',
                woStatusId: 1, //1-Assigned 2-Started 3-Arrived
                woType: 'Refix'
            }]
        });
    mock.onPost(ApiEndpoint.GET_WO_PATH)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS',
            woDetails: {
                estDistance: '2.9 km',
                estTime: '7 mins',
                encryptedPath: 'yybi@ctkfNMs@j@MzA[lEaAbAU`@Ik@cDk@yC_@sCqAuHqE`A{D~@cKzB}@H]?sCSwAIg@Ig@e@a@e@kDrAy@\\}HvCeDlAwEdB}D|AeAl@A`@C|AEbDCxCAvDEtDK~@DDBNGNSHI?ICM@UBO?kAPSLOT?FLTBLDVH`@',
                geoPointsString: '[Latitude:6.90093|Longitude:79.8549, Latitude:6.90096|Longitude:79.855, Latitude:6.901|Longitude:79.85516, Latitude:6.901|Longitude:79.85516, Latitude:6.9009|Longitude:79.85519, Latitude:6.90078|Longitude:79.85523, Latitude:6.90065|Longitude:79.85527, Latitude:6.90058|Longitude:79.85529, Latitude:6.90048|Longitude:79.85532, Latitude:6.90032|Longitude:79.85537, Latitude:6.90015|Longitude:79.85543, Latitude:6.90005|Longitude:79.85546, Latitude:6.89998|Longitude:79.85548, Latitude:6.89982|Longitude:79.85553, Latitude:6.89965|Longitude:79.85558, Latitude:6.89947|Longitude:79.85564, Latitude:6.89946|Longitude:79.85564, Latitude:6.89929|Longitude:79.8557, Latitude:6.89912|Longitude:79.85575, Latitude:6.89895|Longitude:79.85581, Latitude:6.89878|Longitude:79.85586, Latitude:6.89878|Longitude:79.85586, Latitude:6.899|Longitude:79.85668, Latitude:6.89922|Longitude:79.85745, Latitude:6.89938|Longitude:79.85819, Latitude:6.89942|Longitude:79.85837, Latitude:6.89979|Longitude:79.85974, Latitude:6.89979|Longitude:79.85974, Latitude:6.90031|Longitude:79.85957, Latitude:6.90084|Longitude:79.85941, Latitude:6.9014|Longitude:79.85922, Latitude:6.90178|Longitude:79.85909, Latitude:6.90264|Longitude:79.85883, Latitude:6.90317|Longitude:79.85864, Latitude:6.90328|Longitude:79.85861, Latitude:6.90372|Longitude:79.85847, Latitude:6.9038|Longitude:79.85844, Latitude:6.90389|Longitude:79.85843, Latitude:6.90396|Longitude:79.85843, Latitude:6.90403|Longitude:79.85842, Latitude:6.90418|Longitude:79.85842, Latitude:6.90492|Longitude:79.85852, Latitude:6.905|Longitude:79.85852, Latitude:6.90536|Longitude:79.85857, Latitude:6.90556|Longitude:79.85862, Latitude:6.90576|Longitude:79.85881, Latitude:6.90593|Longitude:79.859, Latitude:6.90593|Longitude:79.859, Latitude:6.90679|Longitude:79.85858, Latitude:6.90703|Longitude:79.85845, Latitude:6.90708|Longitude:79.85843, Latitude:6.90806|Longitude:79.85796, Latitude:6.90867|Longitude:79.85767, Latitude:6.909|Longitude:79.85752, Latitude:6.90911|Longitude:79.85747, Latitude:6.9093|Longitude:79.85738, Latitude:6.9095|Longitude:79.85728, Latitude:6.90968|Longitude:79.85719, Latitude:6.9099|Longitude:79.85709, Latitude:6.91001|Longitude:79.85704, Latitude:6.91058|Longitude:79.85677, Latitude:6.91082|Longitude:79.85665, Latitude:6.91084|Longitude:79.85664, Latitude:6.91093|Longitude:79.8566, Latitude:6.91153|Longitude:79.8563, Latitude:6.91188|Longitude:79.85607, Latitude:6.91188|Longitude:79.85607, Latitude:6.91189|Longitude:79.8559, Latitude:6.9119|Longitude:79.85582, Latitude:6.91191|Longitude:79.85543, Latitude:6.91193|Longitude:79.85515, Latitude:6.91194|Longitude:79.8549, Latitude:6.91194|Longitude:79.85481, Latitude:6.91194|Longitude:79.85461, Latitude:6.91195|Longitude:79.85442, Latitude:6.91195|Longitude:79.85423, Latitude:6.91196|Longitude:79.85403, Latitude:6.91196|Longitude:79.85384, Latitude:6.91196|Longitude:79.85364, Latitude:6.91197|Longitude:79.85308, Latitude:6.91197|Longitude:79.85307, Latitude:6.91197|Longitude:79.85292, Latitude:6.91197|Longitude:79.85278, Latitude:6.91198|Longitude:79.85254, Latitude:6.91199|Longitude:79.85224, Latitude:6.912|Longitude:79.85201, Latitude:6.91202|Longitude:79.8519, Latitude:6.91204|Longitude:79.85182, Latitude:6.91206|Longitude:79.85169, Latitude:6.91206|Longitude:79.85169, Latitude:6.91205|Longitude:79.85168, Latitude:6.91204|Longitude:79.85168, Latitude:6.91204|Longitude:79.85167, Latitude:6.91203|Longitude:79.85166, Latitude:6.91202|Longitude:79.85163, Latitude:6.91201|Longitude:79.85161, Latitude:6.91201|Longitude:79.85158, Latitude:6.91201|Longitude:79.85156, Latitude:6.91202|Longitude:79.85154, Latitude:6.91204|Longitude:79.85151, Latitude:6.91205|Longitude:79.8515, Latitude:6.91208|Longitude:79.85147, Latitude:6.91211|Longitude:79.85146, Latitude:6.91213|Longitude:79.85145, Latitude:6.91215|Longitude:79.85145, Latitude:6.91217|Longitude:79.85145, Latitude:6.9122|Longitude:79.85145, Latitude:6.91225|Longitude:79.85147, Latitude:6.91232|Longitude:79.85146, Latitude:6.91236|Longitude:79.85145, Latitude:6.91243|Longitude:79.85144, Latitude:6.91251|Longitude:79.85144, Latitude:6.91289|Longitude:79.85135, Latitude:6.91295|Longitude:79.8513, Latitude:6.91299|Longitude:79.85128, Latitude:6.91302|Longitude:79.85124, Latitude:6.91307|Longitude:79.85117, Latitude:6.91307|Longitude:79.85117, Latitude:6.91307|Longitude:79.85115, Latitude:6.91307|Longitude:79.85114, Latitude:6.91307|Longitude:79.85113, Latitude:6.91305|Longitude:79.8511, Latitude:6.91303|Longitude:79.85107, Latitude:6.91301|Longitude:79.85105, Latitude:6.913|Longitude:79.85102, Latitude:6.91299|Longitude:79.85098, Latitude:6.91298|Longitude:79.85095, Latitude:6.91299|Longitude:79.85091, Latitude:6.91295|Longitude:79.85083, Latitude:6.9129|Longitude:79.85066]'
            }
        });
    mock.onPost(ApiEndpoint.UPDATE_ASSIGNED_JOB)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS',
        });
    mock.onPost(ApiEndpoint.CALL_LOG)
        .reply(200, {
            status: 1,
            statusDescription: 'SUCCESS',
        });
}