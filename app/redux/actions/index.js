// combining all action creators
import * as JobActions from './JobActions';
import * as AppActions from './AppActions';

export const ActionCreators = Object.assign(
    {},
    JobActions,
    AppActions
);
