import * as types from './types';

export function getUserStatusAction() {
    return {
        type: types.USER_ONLINE_STATUS
    };
}

export function storeUserStatusAction(status) { // Status is a string. online or offline
    return {
        type: types.STORE_USER_ONLINE_STATUS,
        payload: status
    };
}

export function changeUserStatusOnBackend(status) { // Status is a string. online or offline
    return {
        type: types.CHANGE_USER_ONLINE_STATUS_BACKEND,
        payload: status
    };
}

export function getUpcomingJobsAction() {
    return {
        type: types.GET_UPCOMING_JOBS
    };
}

export function assignJobAction(payload) {
    return {
        type: types.ASSIGN_A_JOB,
        payload
    };
}

export function getMarkerDetails(id) {
    return {
        type: types.GET_MARKER_DETAILS,
        payload: id
    };
}

export function getAssignedJobs() {
    return {
        type: types.GET_ASSIGNED_JOBS,
    };
}

export function getWODetails(id) {
    return {
        type: types.GET_WO_PATH_DETAILS,
        payload: id
    };
}

export function updateWOStatus(workorder) {
    return {
        type: types.UPDATE_ASSIGNED_JOB,
        payload: workorder
    };
}

export function callLog(payload) {
    return {
        type: types.PUT_CALL_LOG,
        payload
    };
}