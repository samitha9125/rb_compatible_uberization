import * as types from './types';

export function changeCurrentScreen(screen) {
    return {
        type: types.CHANGE_CURRENT_PAGE,
        payload: screen
    };
}

export function showAlert(payload) {
    return {
        type: types.SHOW_ALERT,
        payload: payload
    };
}

export function hideAlert() {
    return {
        type: types.HIDE_ALERT,
    };
}

export function changeUserOnlineStatus(status) { // Online status = true. Offline otherwise
    return {
        type: types.CHANGE_USER_ONLINE_STATUS,
        payload: status
    };
}

export function heartBeat() { // Online status = true. Offline otherwise
    return {
        type: types.HEART_PULSE,
    };
}

export function changeAppNetworkStatus(payload) { // Online status = true. Offline otherwise
    return {
        type: types.CHANGE_APP_NETWORK_STATUS,
        payload
    };
}