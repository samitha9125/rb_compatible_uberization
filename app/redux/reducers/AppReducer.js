import createReducer from '../../utils/createReducer';
import * as types from '../actions/types';
import { Constants } from '../../config';

const initialState = {
    selectedLanguage: 'en',
    pmsId: 105,
    currentPage: Constants.screenTypes.MyJobs,
    userOnline: true,
    userProfile: {
        name: 'Sameera Perera',
        total: 12500,
        totalJobs: 34,
        totalHours: '40 hrs 24min',
        totalDistance: 81.3
    },

    // alert
    show: false,
    type: '',
    title: '',
    message: '',
    positiveButton: true,
    positiveButtonActionType: '',
    positiveButtonText: 'Yes',
    negativeButton: false,
    negativeButtonText: '',
    negativeButtonActionType: '',

    // App online status
    isAppOnline: true
};

export const appReducer = createReducer(initialState, {
    [types.LANGUAGE_CHANGED](state, action) {
        return { ...state, selectedLanguage: action.payload };
    },
    [types.CHANGE_CURRENT_PAGE](state, action) {
        return { ...state, currentPage: action.payload };
    },
    [types.CHANGE_USER_ONLINE_STATUS](state, action) {
        return { ...state, userOnline: action.payload };
    },
    [types.SHOW_ALERT](state, action) {
        return { ...state, ...action.payload };
    },
    [types.HIDE_ALERT](state) {
        return {
            ...state,
            show: false,
            type: '',
            title: '',
            message: '',
            positiveButton: true,
            positiveButtonActionType: '',
            positiveButtonText: 'Yes',
            negativeButton: false,
            negativeButtonText: '',
            negativeButtonActionType: '',
        };
    },
    [types.CHANGE_APP_NETWORK_STATUS](state, action) {
        return {
            ...state,
            isAppOnline: action.payload // action.payload is either true or false.
        }
    }
});
