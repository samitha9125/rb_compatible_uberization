/**
 * Loading reducer made separate for easy blacklisting
 * Avoid data persist
 */
import createReducer from '../../utils/createReducer';
import * as types from '../actions/types';

const initialState = {
    userOnlineStatus: '',
    isUserOnAJob: false, // Whether user is currently performing a job or not.
    teamLocation: {},
    fetchingUserStatus: true,
    jobsList: [],
    completedJobsList: [
        {
            id: 1,
            type: 'Delivery',
            category: 'TV',
            stars: 5,
            job_id: '12345',
            completedDateTime: '22/09/2018  02:01 PM'

        }
    ],
    selectedAssignedWO: {
        path: {},
        loading: true,
        woId: null,
        showPath: false
    },
    upcomingJobsList: [],
    selectedMarkerDetails: {},
    isLoadingMarkerdetails: true,
    fetchingCompletedJobs: false
};

export const jobReducer = createReducer(initialState, {
    [types.LANGUAGE_CHANGED](state, action) {
        return { ...state, selectedLanguage: action.payload };
    },
    [types.STORE_UPCOMING_JOBS](state, action) {
        return { ...state, upcomingJobsList: action.payload };
    },
    [types.STORE_MARKER_DETAILS](state, action) {
        return { ...state, isLoadingMarkerdetails: false, selectedMarkerDetails: action.payload };
    },
    [types.FETCHING_MARKER_DETAILS](state) {
        return { ...state, isLoadingMarkerdetails: true };
    },
    [types.STORE_USER_ONLINE_STATUS](state, action) {
        return { ...state, userOnlineStatus: action.payload };
    },
    [types.STORE_ASSIGNED_JOBS](state, action) {
        return { ...state, jobsList: action.payload };
    },
    [types.UPDATE_USER_JOB_PERFORMING_STATUS](state, action) {
        return { ...state, isUserOnAJob: action.payload };
    },
    [types.STORE_TEAM_LOCATION](state, action) {
        return { ...state, teamLocation: action.payload };
    },
    [types.STORE_ASSIGNED_WO_DETAILS](state, action) {
        return {
            ...state, selectedAssignedWO: {
                ...state.selectedAssignedWO,
                path: { ...action.payload },
                woId: action.payload.woId,
                showPath: true,
                loading: false
            }
        };
    },
    [types.FETCHING_USER_ONLINE_STATUS](state, action) {
        return { ...state, fetchingUserStatus: action.payload };
    },
    [types.STORE_ASSIGNED_WO_ID](state, action) {
        return {
            ...state, selectedAssignedWO: {
                ...state.selectedAssignedWO,
                woId: action.payload,
            }
        };
    },
});
