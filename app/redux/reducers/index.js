/* 
 * combines all th existing reducers
 */
import * as JobReducer from './JobReducer';
import * as AppReducer from './AppReducer';

export default Object.assign(JobReducer, AppReducer);
