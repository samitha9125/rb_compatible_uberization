/*
 * Color object to provide colors throughout the app.
 */
const Images = {
    icons: {
        ic_home: require('../assets/Images/icons/ic_home/home.png'),
        ic_star: require('../assets/Images/icons/ic_star/star.png'),
        ic_get_location: require('../assets/Images/icons/ic_get_location/FAB.png'),
        ic_user_location: require('../assets/Images/icons/ic_user_location/person.png'),
        ic_tv: require('../assets/Images/icons/ic_tv/tv.png'),
        ic_phone: require('../assets/Images/icons/ic_phone/mobile.png'),
        ic_router: require('../assets/Images/icons/ic_router/wifi.png'),
        ic_search: require('../assets/Images/icons/ic_search/search.png'),
        ic_call: require('../assets/Images/icons/ic_call/tp.png'),
        ic_user: require('../assets/Images/icons/ic_user/user.png'),
        ic_location_pointer: require('../assets/Images/icons/ic_lp/pfp.png'),
        ic_warning: require('../assets/Images/icons/ic_warning/attention1.png'),
        ic_error: require('../assets/Images/icons/ic_error/error.png'),
        ic_distance: require('../assets/Images/icons/ic_distance/distance.png'),
        ic_archive: require('../assets/Images/icons/ic_archive/archive-black-box.png'),
        ic_clock: require('../assets/Images/icons/ic_clock/clock.png'),

        tab_job_enabled: require('../assets/Images/icons/tab_job_enabled/colour.png'),
        tab_job_disabled: require('../assets/Images/icons/tab_job_disabled/colour.png'),
        tab_completed_enabled: require('../assets/Images/icons/tab_completed_enabled/oval.png'),
        tab_completed_disabled: require('../assets/Images/icons/tab_completed_disabled/oval.png'),
        tab_upcoming_enabled: require('../assets/Images/icons/tab_upcoming_enabled/map.png'),
        tab_upcoming_disabled: require('../assets/Images/icons/tab_upcoming_disabled/oval.png'),
        tab_profile_enabled: require('../assets/Images/icons/tab_profile_enabled/grey.png'),
        tab_profile_disabled: require('../assets/Images/icons/tab_profile_disabled/grey.png'),
    },
    map_marker: {
        marker_tv: require('../assets/Images/Map_markers/marker_tv/dtv.png'),
        marker_phone: require('../assets/Images/Map_markers/marker_phone/mobile.png'),
        marker_router: require('../assets/Images/Map_markers/marker_router/HBB.png'),
        default_marker: require('../assets/Images/Map_markers/default_marker/mobile.png'),
    },
    gifs: {
        loader: require('../assets/Images/Gifs/Loader/loader.gif'),
    }

};
export { Images };
