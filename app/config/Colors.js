/*
 * Color object to provide colors throughout the app.
 */
const Colors = {
    COLOR_PRIMARY: '#61206C',
    COLOR_BLACK: '#000000',
    COLOR_WHITE: '#FFFFFF',
    COLOR_GRAY: '#757575',
    COLOR_GRAY_DARK: '#565654',
    COLOR_HYPERLINKED_BLUE: '#0751C9',
    COLOR_POLYLINE_BLUE: '#378AF7',
    COLOR_SECONDARY:'#F8931D',
    COLOR_YELLOW:'#FFC400',
    COLOR_GREEN:'#54ab8a',
    COLOR_DARK_GREEN:'#25995D',
    COLOR_ERROR:'#FF001F',
    COLOR_PINK:'#EB4869',
    COLOR_SNACK_BAR:'#2f2f2f',
    COLOR_GRAY20:'#333333',
    
    COLOR_GRAY_LIGHT:'rgba(117, 117, 117, 0.2)',
    COLOR_GRAY_DARK_LIGHT:'rgba(20, 20, 20, 0.7)',
    COLOR_BLACK_LIGHT:'rgba(0, 0, 0, 0.6)',
    COLOR_TRANSPARENT:'rgba(0, 0, 0, 0.0)',
};
export { Colors };
