const Constants = {
    TabNavigator: 'TabNavigator',
    screenTypes: {
        MyJobs: 'MyJobs',
        Completed: 'Completed',
        Upcoming: 'Upcoming',
        Profile: 'Profile'
    },
    productCategories: {
        TV: 'TV',
        Phone: 'Phone',
        Router: 'Router'
    },
    alertType:{
        WARNING:'WARNING',
        ERROR:'ERROR',
    },
    alertActionTypes:{
        PROCEED_TO_ASSIGN_JOB:'PROCEED_TO_ASSIGN_JOB',
        SOMETHING_WENT_WRONG:'SOMETHING_WENT_WRONG',
    },
    jobStatus:{
        ASSIGNED:'Assigned',
        STARTEDTRAVEL:'StartTravel',
        ARRIVED:'Arrived',
        PROCEEDJOB:'ProceedJob',
    },
    userStatus:{
        ONLINE:'Online',
        OFFLINE:'Offline'
    }
}

export { Constants }