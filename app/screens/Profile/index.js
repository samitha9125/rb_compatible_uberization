import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native'
import { Constants, Colors, Images } from '../../config';
import { connect } from 'react-redux';
import * as appActions from '../../redux/actions/AppActions';
import Button from '../../components/Button';
import { getLocalizedString } from '../../utils/Localizer';
import { Text, Subtitle, LargeMediumHeading, Caption } from '../../components/Typography';
import { rupeeFormat, centsFormat } from '../../utils/StringUtil';

const DAY = 'DAY';
const WEEK = 'WEEK';
const MONTH = 'MONTH';
class index extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            tabBarOnPress({ navigation, defaultHandler }) {
                params.onChangeScreen(Constants.screenTypes.Profile);
                defaultHandler();
            }
        };
    };

    constructor() {
        super();
        this.localization = getLocalizedString('en', Constants.screenTypes.Profile);
        this.state = {
            filteration: DAY
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({
            onChangeScreen: this.onChangeScreen,
        });
    }

    onChangeScreen = (screen) => {
        this.props.changeScreen(screen);
    }

    onPressFilteration = (filter) => {
        this.setState({ filteration: filter });
    }

    render() {
        const { userOnline, userProfile } = this.props;
        const { filteration } = this.state;

        const description = userOnline ? null : <Text style={styles.description}>{this.localization.offlineDescription}</Text>
        return (
            <ScrollView bounces={false} contentContainerStyle={styles.container}>
                <View style={styles.buttonContainer}>
                    <Button
                        style={userOnline ? styles.online : styles.offline}
                        pressed={this.props.goOfflinePressed}
                        childrenStyle={styles.buttonText}
                        weight='bold'
                        disabled={!userOnline}>
                        {userOnline ? this.localization.goOffline : this.localization.offline}</Button>
                    {description}
                </View>
                <View style={styles.profileContainer}>
                    <Subtitle style={StyleSheet.flatten(styles.name)}>{this.props.userProfile.name}</Subtitle>
                    <View style={styles.buttonArray}>
                        <Button
                            style={filteration === DAY ? styles.filterationEnabledButton : styles.filterationDisabledButton}
                            pressed={() => this.onPressFilteration(DAY)}
                            labelSize={true}
                            childrenStyle={filteration === DAY ? styles.textEnabled : styles.textDisabled}>
                            {this.localization.day}</Button>
                        <Button
                            style={filteration === WEEK ? styles.filterationEnabledButton : styles.filterationDisabledButton}
                            pressed={() => this.onPressFilteration(WEEK)}
                            labelSize={true}
                            childrenStyle={filteration === WEEK ? styles.textEnabled : styles.textDisabled}>
                            {this.localization.week}</Button>
                        <Button
                            style={filteration === MONTH ? styles.filterationEnabledButton : styles.filterationDisabledButton}
                            pressed={() => this.onPressFilteration(MONTH)}
                            labelSize={true}
                            childrenStyle={filteration === MONTH ? styles.textEnabled : styles.textDisabled}>
                            {this.localization.month}</Button>
                    </View>
                    <Text style={StyleSheet.flatten(styles.total)} align='center'>
                        {this.localization.rs}<LargeMediumHeading style={StyleSheet.flatten(styles.textEnabled)} weight='bold'>
                            {rupeeFormat(userProfile.total) + '' + centsFormat(userProfile.total)}
                        </LargeMediumHeading>
                    </Text>
                    <Text style={StyleSheet.flatten(styles.textDesc)} align='center'> {this.localization.totalDescription + ' 7 ' + filteration.toLowerCase() + 's'}</Text>
                    <View style={styles.detailsContainer}>
                        <View style={styles.comp}>
                            <Image style={styles.image} source={Images.icons.ic_home} />
                            <Text weight='bold'>{userProfile.totalJobs}</Text>
                            <Text weight='light' style={StyleSheet.flatten(styles.compTitle)}>{this.localization.totalJobs}</Text>
                        </View>
                        <View style={styles.comp}>
                            <Image style={styles.image} source={Images.icons.ic_home} />
                            <Text weight='bold'>{userProfile.totalHours}</Text>
                            <Text weight='light' style={StyleSheet.flatten(styles.compTitle)}>{this.localization.HoursOnline}</Text>
                        </View>
                        <View style={styles.comp}>
                            <Image style={styles.image} source={Images.icons.ic_home} />
                            <Text weight='bold'>{userProfile.totalDistance}</Text>
                            <Text weight='light' style={StyleSheet.flatten(styles.compTitle)}>{this.localization.totalDistance}</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1
    },
    online: {
        width: '50%',
        backgroundColor: Colors.COLOR_SECONDARY,
        borderRadius: 8
    },
    offline: {
        width: '50%',
        borderRadius: 4,
        opacity: 0.5
    },
    buttonContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 44,
    },
    buttonText: {
        color: Colors.COLOR_WHITE
    },
    description: {
        color: Colors.COLOR_GRAY,
        marginTop: 12
    },
    profileContainer: {
        margin: 10,
        marginTop: 30,
        padding: 13,
        backgroundColor: Colors.COLOR_WHITE,
        elevation: 5,
        shadowRadius: 5,
        shadowOpacity: 0.2,
        shadowColor: Colors.COLOR_BLACK,
        shadowOffset: {
            width: 0,
            height: 0
        },
        width: '95%',
        borderRadius: 8
    },
    name: {
    },
    buttonArray: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 30
    },
    filterationEnabledButton: {
        backgroundColor: Colors.COLOR_WHITE,
        borderColor: Colors.COLOR_SECONDARY,
        borderWidth: 1,
        borderRadius: 20,
        width: '27%',
        height: 30
    },
    filterationDisabledButton: {
        backgroundColor: Colors.COLOR_WHITE,
        borderColor: Colors.COLOR_GRAY,
        borderWidth: 1,
        borderRadius: 20,
        width: '27%',
        height: 30
    },
    textEnabled: {
        color: Colors.COLOR_SECONDARY
    },
    total: {
        color: Colors.COLOR_SECONDARY,
        marginTop: 30
    },
    textDisabled: {
        color: Colors.COLOR_GRAY
    },
    textDesc: {
        color: Colors.COLOR_GRAY,
        paddingTop: 8
    },
    detailsContainer: {
        backgroundColor: Colors.COLOR_SECONDARY,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginTop: 15,
        paddingVertical: 20,
        borderRadius: 8,
    },
    comp: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        tintColor: Colors.COLOR_WHITE,
        marginBottom: 10,
        width: 24,
        height: 24
    },
    compTitle: {
        marginTop: 10,
        color: Colors.COLOR_WHITE
    }
})

const mapStateToProps = state => {
    return {
        userOnline: state.appReducer.userOnline,
        userProfile: state.appReducer.userProfile,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        changeScreen: (screen) => dispatch(appActions.changeCurrentScreen(screen)),
        goOfflinePressed: () => dispatch(appActions.changeUserOnlineStatus(false)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(index);