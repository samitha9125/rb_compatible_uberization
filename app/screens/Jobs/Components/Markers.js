import React from 'react';
import { Image, StyleSheet, Text } from 'react-native';
import { Images, Constants } from '../../../config';

/**
 * 
 * !!!! IMPORTANT !!!!!
 * Here Image is wrapped by a Text because otherwise Image will not be visible at all.
 * Wrapping the Image with a Text makes image visible
 * 
 */

const UserLocationMark = () => {
    return (
        <Text><Image style={StyleSheet.flatten(styles.usrimg)} source={Images.icons.ic_user_location} /></Text>
    );
};

const GetMyLocationMark = () => {
    return (
        <Image style={styles.image} source={Images.icons.ic_get_location} />
    );
};

const DeliveryMarker = () => {
    return (
        <Text><Image style={StyleSheet.flatten(styles.img)} source={Images.map_marker.default_marker} /></Text>
    );
}

const LocationMark = (props) => {
    let image = '';
    switch (props.markerType) {
        case 1://Constants.productCategories.Phone:
            image = Images.map_marker.marker_phone;
            break;
        case 2://Constants.productCategories.TV:
            image = Images.map_marker.marker_tv;
            break;
        case 3://Constants.productCategories.Router:
            image = Images.map_marker.marker_router;
            break;
    }
    return (
        <Text><Image onLoad={() => this.forceUpdate()} style={StyleSheet.flatten(styles.img)} source={image} /></Text>
    );
};

const styles = StyleSheet.create({
    img: {
        width: 56,
        height: 68
    },
    usrimg: {
        width: 42,
        height: 96,
    },
    image: {
        width: 56,
        height: 56,
    }
});

export { UserLocationMark, GetMyLocationMark, LocationMark, DeliveryMarker };


