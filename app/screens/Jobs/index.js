import React, { Component } from 'react';
import { Switcher } from './Tabs/Switcher';
import { connect } from 'react-redux';
import * as appActions from '../../redux/actions/AppActions';
import * as jobActions from '../../redux/actions/JobActions';
import { Constants } from '../../config';
import NavigationService from '../../navigation/NavigationService';
class index extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            tabBarOnPress({ navigation, defaultHandler }) {
                params.onChangeScreen(Constants.screenTypes.MyJobs);
                defaultHandler();
            }
        };
    };

    constructor() {
        super();
    }

    state = {
        listOfJobs: [],
    }

    componentDidMount() {
        this.props.navigation.setParams({
            onChangeScreen: this.changeScreen,
        });
        this.props.getUserStatus();
    }

    changeScreen = (screen) => {
        this.props.changeScreen(screen);
    }

    onNavBack = () => {
        this.props.navigation.goBack();
    }

    onPressButtonSearch = () => {

    }

    storeUserStatus = (status) => {
        if (status === Constants.userStatus.ONLINE) {
            this.props.changeUserStatusOnBackend(status);
        }
    }

    render() {
        return (
            <Switcher
                ref={ref => NavigationService.setMyJobNavigator(ref)}
                screenProps={{
                    listOfJobs: this.props.jobList,
                    onlineStatus: this.props.onlineStatus,
                    selectedWO: this.props.selectedAssignedWO,
                    storeUserStatusAction: this.storeUserStatus,
                    fetchingUserStatus: this.props.fetchingUserStatus,
                    changeWoStatus: this.props.changeWoStatus,
                    isUserOnaJob: this.props.isUserOnaJob,
                    teamLocation: this.props.teamLocation,
                    callLog: this.props.callLog
                }}
            />
        );
    }

}

const mapStateToProps = state => {
    return {
        jobList: state.jobReducer.jobsList,
        onlineStatus: state.jobReducer.userOnlineStatus,
        selectedAssignedWO: state.jobReducer.selectedAssignedWO,
        fetchingUserStatus: state.jobReducer.fetchingUserStatus,
        isUserOnaJob: state.jobReducer.isUserOnAJob,
        teamLocation: state.jobReducer.teamLocation,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        changeScreen: (screen) => dispatch(appActions.changeCurrentScreen(screen)),
        getUserStatus: () => dispatch(jobActions.getUserStatusAction()),
        changeWoStatus: (wo) => dispatch(jobActions.updateWOStatus(wo)),
        changeUserStatusOnBackend: (payload) => dispatch(jobActions.changeUserStatusOnBackend(payload)),
        callLog: (payload) => dispatch(jobActions.callLog(payload)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(index);

