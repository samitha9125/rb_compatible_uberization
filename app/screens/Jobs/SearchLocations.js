import React, { Component } from 'react';
import { FlatList, StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as loanActions from '../../../../../../redux/actions/LoanActions';
import { Colors, Images } from '../../../../../../config';
import { Caption, Text } from '../../../../../../components/Typography';
import { SearchHeader } from '../../../../../../navigation/SearchHeader';

class SearchLocations extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        const header = <SearchHeader
            onBackPress={params.onPressBack}
            placeholder={'Search by location'}
            searchedText={params.onSearchText}
            paddingBottom={20}
            paddingLeft={15}
            elevation={5}
            statusBarHeight={params.statusBarHeight} />;
        return {
            header: header
        };
    };

    constructor(props) {
        super(props);
        this.locationsStringArr = [];
        this.locationsArray = [];
        this.count = 0;
        props.arcades.forEach(arcade => {
            if (this.locationsStringArr.indexOf(arcade.location.trim()) === -1) {
                this.locationsStringArr.push(arcade.location.trim());
                this.count++;
            }
        });
        this.locationsStringArr.forEach(location => {
            this.locationsArray.push({ key: this.count, name: location });
            this.count++;
        });
        this.state = {
            districts: this.locationsArray,
            districtsTemp: this.locationsArray
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({
            onPressBack: this.onNavBack,
            onSearchText: this.onTypeOnSearch,
            statusBarHeight: this.props.statusBarHeight,
        });
    }

    onNavBack = () => {
        this.props.navigation.goBack();
    }

    onTypeOnSearch = (text) => {
        let districtsTemp = this.state.districts.filter(district =>
            district.name.toLowerCase().indexOf(text.toLowerCase()) !== -1);
        this.setState({ text, districtsTemp });
    }

    getLocationList = (key) => {
        let district = this.state.districts.find(district => district.key === key);
        this.props.searchArcadeDetails({ query: district.name });
    }

    renderItem = ({ item }) => (
        <TouchableOpacity style={styles.singleItem} onPress={() => this.getLocationList(item.key)}>
            <Image source={Images.icons.ic_location} style={styles.icon} />
            <Caption>{item.name}</Caption>
        </TouchableOpacity>
    )

    separator = () => (
        <View style={styles.line} />
    )

    render() {
        const NO_ARCADE = <View style={styles.emptyContainer}>
            <Text>No arcades found.</Text>
        </View>;
        const body = this.props.arcades.length > 0 ?
            <FlatList
                data={this.state.districtsTemp}
                style={styles.container}
                renderItem={this.renderItem}
                ItemSeparatorComponent={this.separator}
                ListFooterComponent={this.separator}
                keyExtractor={item => item.key.toString()}
            /> :
            NO_ARCADE;

        return (
            <View style={styles.mainContainer}>
                {body}
            </View>
        );
    }
}

SearchLocations.propTypes = {
    navigation: PropTypes.object,
    searchArcadeDetails: PropTypes.func,
    statusBarHeight: PropTypes.number,
    arcades: PropTypes.array,
};

const mapStateToProps = (state) => {
    return {
        arcades: state.arcadeReducer.allArcades.DATA,
        statusBarHeight: state.appReducer.statusBarHeight,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        searchArcadeDetails: (payload) => dispatch(loanActions.getSearchedArcadesAction(payload)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchLocations);

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: Colors.COLOR_WHITE,
    },
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.COLOR_WHITE,
    },
    line: {
        backgroundColor: Colors.COLOR_GREY,
        height: 1
    },
    singleItem: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 15,
        paddingLeft: 25,
    },
    icon: {
        width: 14,
        height: 20,
        tintColor: Colors.COLOR_GREY_LIGHT,
        marginRight: 9
    },
    emptyContainer: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
    }
});