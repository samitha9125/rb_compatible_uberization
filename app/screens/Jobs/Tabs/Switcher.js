import { createMaterialTopTabNavigator } from 'react-navigation';
import OutletMap from '../Tabs/OutletMap';
import OutletList from '../Tabs/OutletList';
import normalize from '../../../helpers/normalizeText';
import { Colors } from '../../../config';

const HeaderTabNav = createMaterialTopTabNavigator(
    {
        Map: {
            screen: OutletMap,
            navigationOptions: ({ navigation }) => ({
                title: 'MAP VIEW',
            })
        },
        List: {
            screen: OutletList,
            navigationOptions: ({ navigation }) => ({
                title: 'LIST VIEW',
            })
        },
    },
    {
        tabBarOptions:
        {
            style: {
                elevation: 5,
                backgroundColor: Colors.COLOR_PRIMARY,
                shadowRadius: 5,
                shadowOpacity: 0.2,
                shadowColor: Colors.COLOR_BLACK,
                shadowOffset: {
                    width: 0,
                    height: 5
                },
            },
            indicatorStyle: {
                backgroundColor: Colors.COLOR_WHITE,
                borderBottomWidth: 3,
                borderBottomColor: Colors.COLOR_WHITE
            },
            labelStyle: {
                color: Colors.COLOR_WHITE,
                fontWeight: 'normal',
                fontSize: normalize(14)
            },
            upperCaseLabel: false
        },
        initialRouteName: 'Map',
        lazy: true,
        swipeEnabled: true,
        optimizationsEnabled: true,
    },
);


const Switcher = HeaderTabNav;
export { Switcher };