import React, { Component } from 'react';
import { FlatList, StyleSheet, View, Image, Linking, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { Colors, Images, Constants } from '../../../config';
import { Text, Subtitle } from '../../../components/Typography';
import Button from '../../../components/Button';

export default class OutletList extends Component {

    static navigationOptions = ({ navigation }) => {
        /**
         * Couldn't use params here since this is a nested navigator and this version of React Navigation
         * Has an issue with setting params to child navigator. Thus as a solution, used getSceenProps function.
         */
        const screenProps = navigation.getScreenProps();
        return {
            tabBarOnPress({ navigation, defaultHandler }) {
                if (screenProps.onlineStatus === Constants.userStatus.ONLINE) {
                    defaultHandler();
                }
            }
        };
    };

    componentDidMount() {
        this.props.navigation.setParams({
            onlineStatus: this.props.screenProps.onlineStatus
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.screenProps.onlineStatus !== this.props.screenProps.onlineStatus) {
            this.props.navigation.setParams({
                onlineStatus: this.props.screenProps.onlineStatus
            });
        }
    }

    onPressCallNumber = (cxMsisdn, woId) => {
        const obj = {
            woId,
            callInfo: {
                cxMsisdn
            },
            info:''
        };
        this.props.screenProps.callLog(obj);

        if (Platform.OS === 'android')
            Linking.openURL(`tel:${cxMsisdn}`);
        else
            Linking.openURL(`tel://${cxMsisdn}`);
    }

    renderItem = ({ item }) => (
        <SingleJobCard
            item={item}
            onPressCallNumber={(number) => this.onPressCallNumber(number, item.woId)}
            onPressProceed={this.onPressProceed}
            isUserOnAJob={this.props.screenProps.isUserOnaJob}
        />
    )

    separator = () => (
        <View style={styles.line} />
    )

    onPressReportIssue = () => {

    }

    onPressProceed = (workOrder) => {
        this.props.screenProps.changeWoStatus({ workOrder, status: Constants.jobStatus.STARTEDTRAVEL });
    }

    render() {
        const { listOfJobs } = this.props.screenProps;
        // Sliced because for 82 test elements, this tends to be stuck.
        const slicedList = listOfJobs.slice(0, 20);

        const NO_JOBS = <View style={styles.emptyContainer}>
            <Subtitle>No Jobs found.</Subtitle>
        </View>;
        const areJobsAvailable = listOfJobs.length > 0;
        const body = areJobsAvailable ?
            <FlatList
                data={slicedList}
                style={styles.container}
                renderItem={this.renderItem}
                ItemSeparatorComponent={this.separator}
                ListFooterComponent={this.separator}
                keyExtractor={item => item.woId.toString()}
                extraData={this.props.screenProps}
            /> :
            NO_JOBS;

        return (
            <View style={styles.mainContainer}>
                {body}
            </View>
        );
    }
}

const SingleJobCard = props => {

    const CX = props.item.cxRequestedTime ?
        <View style={styles.thirdLine}>
            <Image style={StyleSheet.flatten(styles.userIcon)} source={Images.icons.ic_user} />
            <Text
                style={StyleSheet.flatten(styles.cusAddress)}
                weight='light'>
                {'Cx requested time ' + props.item.cxRequestedTime}
            </Text>
        </View> : null;

    let categoryImage = null;
    let typeIcon = null;
    let continueButtonLabel = null;

    switch (props.item.productCategoryId) {
        case 1:
            categoryImage = Images.icons.ic_phone;
            typeIcon = styles.phoneIcon;
            break;
        case 2:
            categoryImage = Images.icons.ic_tv;
            typeIcon = styles.tvRouterIcons;
            break;
        case 3:
            categoryImage = Images.icons.ic_router;
            typeIcon = styles.tvRouterIcons;
            break;
    }

    switch (props.item.woStatusId) {
        case 1: // Assigned
            continueButtonLabel = 'Start travel'
            break;
        case 2: // Already started. 
            continueButtonLabel = 'Start travel'
            break;
        case 3:
            continueButtonLabel = 'Proceed job'
            break;
    }



    return (
        <View style={styles.cardContainer}>
            <View style={styles.firstLine}>
                <View style={styles.typeIconBackground}>
                    <Image style={StyleSheet.flatten(typeIcon)} source={categoryImage} />
                </View>
                <Subtitle weight='500' style={StyleSheet.flatten(styles.title)}>
                    {props.item.woType.charAt(0).toUpperCase() + props.item.woType.slice(1).toLowerCase()}
                </Subtitle>
            </View>
            <View style={styles.secondLine}>
                <Image style={StyleSheet.flatten(styles.pointerIcon)} source={Images.icons.ic_location_pointer} />
                <Text style={StyleSheet.flatten(styles.customerName)}>
                    {props.item.cxDetails.cxName}
                    <Text
                        style={StyleSheet.flatten(styles.cusAddress)}
                        weight='light'>{', ' + props.item.cxDetails.cxAddress}
                    </Text>
                </Text>
            </View>
            {CX}
            <View style={styles.thirdLine}>
                <Image style={StyleSheet.flatten(styles.callIcon)} source={Images.icons.ic_call} />
                {
                    Array(props.item.cxContactNumbers.length).fill().map((_, i) => i).map(i =>
                        <Text
                            weight='bold'
                            onPress={() => props.onPressCallNumber(props.item.cxContactNumbers[i])}
                            style={StyleSheet.flatten(styles.linked)} key={i}>{props.item.cxContactNumbers[i]}
                        </Text>
                    )
                }
            </View>
            <View style={styles.buttonRow}>
                <Button
                    pressed={this.onPressReportIssue}
                    style={styles.secondButtonStyle}
                    childrenStyle={styles.secondButtonChildStyle}
                    weight='bold'>Report an issue</Button>
                <Button
                    pressed={() => props.onPressProceed(props.item)}
                    style={styles.mainButtonStyle}
                    disabled={props.isUserOnAJob}
                    childrenStyle={styles.mainButtonChildStyle}
                    weight='bold'>{continueButtonLabel}</Button>
            </View>
        </View>
    )

}

OutletList.propTypes = {
    screenProps: PropTypes.object
};

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        backgroundColor: Colors.COLOR_WHITE
    },
    cardContainer: {
        margin: 10,
        padding: 13,
        backgroundColor: Colors.COLOR_WHITE,
        elevation: 5,
        shadowRadius: 5,
        shadowOpacity: 0.2,
        shadowColor: Colors.COLOR_BLACK,
        shadowOffset: {
            width: 0,
            height: 0
        },
        width: '95%',
        borderRadius: 8
    },
    mainContainer: {
        flex: 1,
        backgroundColor: Colors.COLOR_WHITE,
    },
    emptyContainer: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
    },
    firstLine: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    secondLine: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 28,
        width: '90%'
    },
    thirdLine: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 22,
        marginRight: 20
    },
    typeIconBackground: {
        padding: 10,
        marginRight: 10,
        borderRadius: 1000,
        backgroundColor: Colors.COLOR_GRAY_LIGHT
    },
    typeIcon: {
        width: 24,
        height: 24
    },
    locationIcon: {
        width: 24,
        height: 24,
        marginRight: 11
    },
    linked: {
        color: Colors.COLOR_HYPERLINKED_BLUE,
        borderBottomWidth: 1,
        borderBottomColor: Colors.COLOR_HYPERLINKED_BLUE,
        marginRight: 13
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 30
    },
    mainButtonStyle: {
        backgroundColor: Colors.COLOR_SECONDARY,
        width: '48%',
        borderRadius: 4
    },
    mainButtonChildStyle: {
        color: Colors.COLOR_WHITE
    },
    secondButtonStyle: {
        backgroundColor: Colors.COLOR_WHITE,
        width: '48%',
        borderWidth: 1,
        borderColor: Colors.COLOR_SECONDARY,
        borderRadius: 4
    },
    secondButtonChildStyle: {
        color: Colors.COLOR_SECONDARY
    },
    userIcon: {
        width: 18,
        height: 24,
        marginRight: 11
    },
    callIcon: {
        width: 30,
        height: 30,
    },
    pointerIcon: {
        width: 18,
        height: 24,
        marginRight: 11
    },
    title: {
        color: Colors.COLOR_BLACK
    },
    customerName: {
        color: Colors.COLOR_BLACK
    },
    cusAddress: {
        color: Colors.COLOR_GRAY_DARK_LIGHT
    },
    phoneIcon: {
        width: 14,
        height: 24,
        marginHorizontal: 5
    },
    tvRouterIcons: {
        width: 24,
        height: 24
    },
});