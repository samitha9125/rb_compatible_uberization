import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    UIManager,
    findNodeHandle,
    Image
} from 'react-native';
import PropTypes from 'prop-types';
import MapView, { Marker, Polyline } from 'react-native-maps';
import Permissions from 'react-native-permissions'
import {
    UserLocationMark,
    GetMyLocationMark,
    DeliveryMarker
} from '../Components/Markers';
import Button from '../../../components/Button';
import CustomMap from '../../../config/CustomMap.json';
import MapPanel from '../../../components/Panels/MapPanel';
import { Constants, Colors, Images } from '../../../config';
import { Label, Subtitle } from '../../../components/Typography';
import { decode } from '../../../utils/decodeCoordinates';
import { isUnusableObject } from '../../../helpers'

const { width } = Dimensions.get('window');

export default class OutletMap extends Component {

    constructor(props) {
        super(props);
        this.mapRef = React.createRef();
        props.navigation.setParams({
            onTabFocus: this.willFocus,
            forceGoBack: false,
        });
    }

    state = {
        region: {
            latitude: 6.9204128,
            longitude: 79.8555824,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        userRegion: {
            latitude: 6.9204128,
            longitude: 79.8555824,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        coordinates: [],
        mapKey: 1,
        DeliveryMarkerKey: 1000,
        markerID: 0,
        showModal: false,
        userLocationfetching: true,
        /**
         * mapHeight is height of full map
         */
        mapHeight: 0,
        modalHeight: 0,

        // BottomSheet
        showBottomSheet: false
    }

    componentDidMount() {
        /**
         * Timeout uses here to re-mount component (Marker component).This makes all markers visible when map loads.
         * After one second of time, key value of Markers will be changed. Thus component will re mounted. Bug fixed.
         * 
         * Second timeout is to re-render the entire mapView to focus real user location without pressing get user location
         * Button. This is also for a bug fix.
         */

        setTimeout(() => this.setState({ mapKey: 200 }), 1000);
        setTimeout(() => this.getPermissions(), 4000);

        setTimeout(() => {
            UIManager.measure(findNodeHandle(this.mapRef), (frameX, frameY, frameWidth, frameHeight, pageX, pageY) => {
                this.setState({ mapHeight: frameHeight });
            });
        }, 0);

    }

    componentDidUpdate(prevProps) {

        if (prevProps.screenProps.performedSearch !== this.props.screenProps.performedSearch &&
            this.props.screenProps.performedSearch) {
            this.props.navigation.navigate('List');
        }

        if (prevProps.screenProps.selectedWO.path !== this.props.screenProps.selectedWO.path) {
            this.decodeCoordinates(this.props.screenProps.selectedWO.path);
        }

        if (prevProps.screenProps.selectedWO.showPath !== this.props.screenProps.selectedWO.showPath) {
            /**
             * Force navigate to Map view. If user was in List view, This ensures BottomSheet appears in Map View only
             */
            this.props.navigation.navigate('Map');
            this.setState({ showBottomSheet: true });
        }

        if (prevProps.screenProps.teamLocation !== this.props.screenProps.teamLocation) {
            this.onPressGetLocation();
        }

    }

    getPermissions = () => {
        Permissions.check('location').then(response => {
            if (response === 'undetermined') {
                Permissions.request('location').then(response => {
                    if (response === 'authorized')
                        this.onPressGetLocation();
                });
            }
            else if (response === 'authorized') {
                this.onPressGetLocation();
            } else {
                /**
                 * ------- TODO --------
                 * Should tell user to enable locations manually.
                 */
            }
        });
    }

    getCoordinates = () => {
        this.setState({ userLocationfetching: true }, () => {
            try {
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        const userRegion = {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        };
                        this.setState({ userRegion, userLocationfetching: false });
                    },
                    (error) => {
                        console.log('error : ', error);
                    },
                    { enableHighAccuracy: false, timeout: 20000 }
                );
            } catch (e) {
                console.log('e.message : ', e.message);
            }
        })
    }

    onPressGetLocation = () => {
        if (this.props.screenProps.onlineStatus === Constants.userStatus.ONLINE) {
            /**
             * If user is online, team location should show on the map. Otherwise user location should be visible.
             * Also isUnusableObject function is used to check teamLocation is setted in Redux. Otherwise this will 
             * return an empty object.
             */
            
            const { teamLocation } = this.props.screenProps;
            let latitude = 0;
            let longitude = 0;
            if (isUnusableObject(teamLocation)) {
                latitude = 6.9204128;
                longitude = 79.8555824;
            } else {
                latitude = Number(teamLocation.latitude);
                longitude = Number(teamLocation.longitude);
            }

            const userRegion = {
                latitude: latitude,
                longitude: longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            };
            this.setState({ userRegion, userLocationfetching: false, mapKey: 201 });

        } else
            this.getCoordinates();
    }

    decodeCoordinates = (object) => {
        const coordinates = object.encryptedPath ? decode(object.encryptedPath) : [];
        this.setState({ coordinates });
        setTimeout(() => {
            /**
             * Remount DeliveryMarker to make it visible after drawing the path.
             */
            this.setState({ DeliveryMarkerKey: 1001 });
        }, 500);
    }

    onPressGoOnline = () => {
        this.props.screenProps.storeUserStatusAction('Online');
    }

    hideBottomSheet = () => {
        this.setState({ showBottomSheet: false });
    }

    panelHeader = () => {
        const item = this.props.screenProps.listOfJobs.find(item => item.woId === this.props.screenProps.selectedWO.woId);
        let categoryImage = null;
        let typeIcon = null;
        if (item) {
            switch (item.productCategoryId) {
                case 1:
                    categoryImage = Images.icons.ic_phone;
                    typeIcon = styles.phoneIcon;
                    break;
                case 2:
                    categoryImage = Images.icons.ic_tv;
                    typeIcon = styles.tvRouterIcons;
                    break;
                case 3:
                    categoryImage = Images.icons.ic_router;
                    typeIcon = styles.tvRouterIcons;
                    break;
            }
        }

        const content = item ?
            <View style={styles.mainPanelView}>
                <View style={styles.firstLine}>
                    <View style={styles.typeIconBackground}>
                        <Image style={StyleSheet.flatten(typeIcon)} source={categoryImage} />
                    </View>
                    <Subtitle weight='500' style={StyleSheet.flatten(styles.title)}>
                        {item.woType.charAt(0).toUpperCase() + item.woType.slice(1).toLowerCase()}
                    </Subtitle>
                </View>
                <View style={styles.buttonRow}>
                    <Button
                        pressed={this.onPressReportIssue}
                        style={styles.secondButtonStyle}
                        childrenStyle={styles.secondButtonChildStyle}
                        weight='bold'>Report an issue</Button>
                    <Button
                        pressed={() => props.onPressProceed(props.item)}
                        style={styles.mainButtonStyle}
                        disabled={true}
                        childrenStyle={styles.mainButtonChildStyle}
                        weight='bold'>Arrived</Button>
                </View>
            </View> : null;
        return (content);
    }

    render() {
        const { userRegion, region, mapKey, DeliveryMarkerKey } = this.state;
        const { onlineStatus } = this.props.screenProps;

        const onlineStatusView = onlineStatus === Constants.userStatus.ONLINE ?
            <View style={styles.onlineLabelView}>
                <Label weight='light' style={StyleSheet.flatten(styles.onlineLabel)}>{'You’re online'}</Label>
            </View> :
            <Button
                style={styles.offline}
                pressed={this.onPressGoOnline}
                childrenStyle={styles.buttonText}
                /** 
                 *  userLocationFetched says the user location  of current app user. Not the team location.
                 */
                showLoader={this.props.screenProps.fetchingUserStatus && this.state.userLocationfetching}
                weight='bold'>
                {'Go online'}
            </Button>;
        return (
            <View ref={ref => this.mapRef = ref} style={styles.mapContainer}>
                <MapPanel
                    header={this.panelHeader}
                    body={null}
                    show={this.state.showBottomSheet}
                    hideModal={this.hideBottomSheet} />
                <MapView
                    key={mapKey}
                    style={styles.map}
                    showsMyLocationButton={false}
                    showsUserLocation={false}
                    initialRegion={region}
                    region={userRegion}
                    customMapStyle={CustomMap}
                    minZoomLevel={14}
                    onRegionChangeComplete={() => { }}
                    loadingEnabled={true}
                    showsScale={false}
                    showsCompass={false}
                    showsBuildings={false}
                    showsTraffic={false}
                    showsIndoors={false}
                    showsPointsOfInterest={false}>
                    <Marker
                        key={87342892347}
                        coordinate={{
                            longitude: Number(userRegion.longitude),
                            latitude: Number(userRegion.latitude)
                        }}
                        onLoad={() => this.forceUpdate()}
                        onPress={() => { }}>
                        <UserLocationMark onLoad={() => this.forceUpdate()} />
                    </Marker>
                    {
                        this.state.coordinates.length > 0 ?
                            <Marker
                                key={DeliveryMarkerKey}
                                coordinate={{
                                    longitude: this.state.coordinates[this.state.coordinates.length - 1].longitude,
                                    latitude: this.state.coordinates[this.state.coordinates.length - 1].latitude
                                }}
                                onLoad={() => this.forceUpdate()}
                                onPress={() => { }}>
                                <DeliveryMarker onLoad={() => this.forceUpdate()} />
                            </Marker> : null
                    }
                    <Polyline
                        coordinates={
                            this.state.coordinates
                        }
                        strokeWidth={8}
                        strokeColor={Colors.COLOR_POLYLINE_BLUE}
                    />
                </MapView>
                <TouchableOpacity onPress={this.onPressGetLocation} style={styles.currentLocationButton}>
                    <GetMyLocationMark />
                </TouchableOpacity>
                {onlineStatusView}
            </View>
        );
    }
}

OutletMap.propTypes = {
    screenProps: PropTypes.object,
    userRegion: PropTypes.object,
    getCoordinates: PropTypes.func,
    navigation: PropTypes.object,
};

const styles = StyleSheet.create({
    mapContainer: {
        flex: 10,
        width: '100%',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1
    },
    currentLocationButton: {
        position: 'absolute',
        left: width * 0.82,
        right: 10,
        bottom: 14,
        zIndex: 0,
    },
    offline: {
        position: 'absolute',
        bottom: 20,
        width: '40%',
        backgroundColor: Colors.COLOR_SECONDARY,
        borderRadius: 8
    },
    buttonText: {
        color: Colors.COLOR_WHITE
    },
    onlineLabel: {
        color: Colors.COLOR_WHITE,
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    onlineLabelView: {
        position: 'absolute',
        bottom: 20,
        backgroundColor: Colors.COLOR_GREEN,
        borderRadius: 16
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        paddingTop: 10,
        borderTopWidth: 0.5,
        borderTopColor: Colors.COLOR_GRAY_LIGHT

    },
    mainButtonStyle: {
        backgroundColor: Colors.COLOR_SECONDARY,
        width: '48%',
        borderRadius: 4
    },
    mainButtonChildStyle: {
        color: Colors.COLOR_WHITE
    },
    secondButtonStyle: {
        backgroundColor: Colors.COLOR_WHITE,
        width: '48%',
        borderWidth: 1,
        borderColor: Colors.COLOR_SECONDARY,
        borderRadius: 4
    },
    secondButtonChildStyle: {
        color: Colors.COLOR_SECONDARY
    },
    firstLine: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    typeIconBackground: {
        padding: 10,
        marginRight: 10,
        borderRadius: 1000,
        backgroundColor: Colors.COLOR_GRAY_LIGHT
    },
    typeIcon: {
        width: 24,
        height: 24
    },
    locationIcon: {
        width: 24,
        height: 24,
        marginRight: 11
    },
    title: {
        color: Colors.COLOR_BLACK
    },
    phoneIcon: {
        width: 14,
        height: 24,
        marginHorizontal: 5
    },
    tvRouterIcons: {
        width: 24,
        height: 24
    },
    mainPanelView: {
        paddingBottom: 20
    }

});