import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Image, FlatList } from 'react-native'
import { getMonthsWithYear, getDatesOfAMonth, getDayName } from '../../utils/StringUtil';
import { Label, Text, Subtitle, Caption } from '../../components/Typography';
import { Colors, Images, Constants } from '../../config';
import { connect } from 'react-redux';
import * as appActions from '../../redux/actions/AppActions';
class index extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        console.log(params);
        return {
            tabBarOnPress({ navigation, defaultHandler }) {
                params.onChangeScreen(Constants.screenTypes.Completed);
                defaultHandler();
            }
        };
    };

    constructor() {
        super();
        this.state = {
            statingDate: '2017/12/20', // Rider's registration date
            monthsArray: [],
            selectedMonth: 0, // this is an index
            datesArray: [],
            selectedDate: 0 // this is an index. Same as selectedMonth
        }
    }

    componentDidMount() {
        const date = new Date(this.state.statingDate);
        const monthsArray = this.monthsCalculator(date);
        const datesArray = getDatesOfAMonth(monthsArray[monthsArray.length - 1]);
        const currentDateIndex = datesArray.findIndex(val => val === getDayName(new Date().getDay()) + ' ' + new Date().getDate())

        this.setState({
            monthsArray,
            selectedMonth: monthsArray.length - 1,
            datesArray,
            selectedDate: currentDateIndex
        });

        this.props.navigation.setParams({
            onChangeScreen: this.onChangeScreen,
        });
    }

    onChangeScreen = (screen) => {
        this.props.changeScreen(screen);
    }

    monthsCalculator = (startingDate, endingDate = new Date()) => {
        let months;
        months = (endingDate.getFullYear() - startingDate.getFullYear()) * 12;
        months -= startingDate.getMonth();
        months += endingDate.getMonth() + 1;

        return getMonthsWithYear(startingDate, months);
    }

    onPressMonth = (selectedMonth) => {
        this.setState({ selectedMonth }, () => {
            const datesArray = getDatesOfAMonth(this.state.monthsArray[selectedMonth]);
            this.setState({ datesArray });
        })
    }

    onPressDate = (selectedDate) => {
        this.setState({ selectedDate });
    }

    renderItem = ({ item }) => (
        <CompleteJobCard item={item} />
    )


    render() {
        const { monthsArray, selectedMonth, datesArray, selectedDate } = this.state;
        const { jobList, fetching } = this.props;

        const NO_JOBS = <View style={styles.emptyContainer}>
            <Subtitle>No Jobs found.</Subtitle>
        </View>;

        const LOADING = <View style={styles.emptyContainer}>
            <Subtitle>Loading...</Subtitle>
        </View>;

        const areJobsAvailable = jobList.length > 0;
        const body = fetching ? LOADING : areJobsAvailable ?
            <FlatList
                data={jobList}
                style={styles.flatListContainer}
                renderItem={this.renderItem}
                keyExtractor={item => item.id.toString()}
                extraData={this.props}
            /> :
            NO_JOBS;

        return (
            <View style={styles.container}>
                <View style={styles.calenderView}>
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        contentContainerStyle={styles.monthScroller}>
                        {
                            Array(monthsArray.length).fill().map((_, i) => i).map(i =>
                                <MonthDateSelector
                                    key={i}
                                    id={i}
                                    type={MONTH}
                                    monthsArray={monthsArray}
                                    selectedMonth={selectedMonth}
                                    onPressMonth={this.onPressMonth} />
                            )
                        }
                    </ScrollView>
                    <ScrollView horizontal={true} contentContainerStyle={styles.dateScroller}>
                        {
                            Array(datesArray.length).fill().map((_, i) => i).map(i => {
                                return (
                                    <MonthDateSelector
                                        key={i}
                                        id={i}
                                        type={'DAY'}
                                        datesArray={datesArray}
                                        selectedDate={selectedDate}
                                        onPressDate={this.onPressDate} />
                                )
                            })
                        }
                    </ScrollView>
                    {body}
                </View>
            </View>
        );
    }
}

const MONTH = 'MONTH';
const MonthDateSelector = (props) => {
    let type = props.type;
    let touchableStyles = {};
    let text;
    let disable = false;
    if (type === MONTH) {
        touchableStyles = props.selectedMonth === props.id ?
            styles.monthTouchableSelected :
            styles.monthTouchableDefault;
        text =
            <Label style={{ color: props.selectedMonth === props.id ? Colors.COLOR_BLACK : Colors.COLOR_GRAY }}>
                {props.monthsArray[props.id]}
            </Label>
    } else {
        touchableStyles = props.selectedDate === props.id ?
            styles.dateTouchableSelected :
            styles.dateTouchableDefault;
        let dateArr = props.datesArray[props.id].split(' ');

        text =
            <View style={styles.dateTextView}>
                <Text align='center' style={{ color: props.selectedDate === props.id ? Colors.COLOR_BLACK : Colors.COLOR_GRAY }}>
                    {`${dateArr[0]}\n${dateArr[1]}`}
                </Text>
            </View>

    }

    return (
        <TouchableOpacity
            disabled={disable}
            onPress={() => type === MONTH ? props.onPressMonth(props.id) : props.onPressDate(props.id)}
            style={touchableStyles}>
            {text}
        </TouchableOpacity>
    )

}


const CompleteJobCard = props => {

    let image = '';

    switch (props.item.category) {
        case Constants.productCategories.Phone:
            image = Images.icons.ic_phone;
            break;
        case Constants.productCategories.TV:
            image = Images.icons.ic_tv;
            break;
        case Constants.productCategories.Router:
            // image = Images.map_marker.marker_router;
            break;
    }
    return (
        <View style={styles.jobCardContainer}>
            <View style={styles.firstLine}>
                <View style={styles.typeIconBackground}>
                    <Image style={styles.typeIcon} source={image} />
                </View>
                <View>
                    <Caption>{props.item.type}</Caption>
                    <View style={styles.starsRow}>
                        {
                            Array(props.item.stars).fill().map((_, i) => i).map(i => (
                                <Image key={i} style={styles.stars} source={Images.icons.ic_star} />
                            ))
                        }

                    </View>
                    <Text weight='light' style={StyleSheet.flatten(styles.bottomText)}>{props.item.job_id + ' | ' + props.item.completedDateTime}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    flatListContainer: {
        flexGrow: 1,
        backgroundColor: Colors.COLOR_WHITE
    },
    calenderView: {

    },
    monthScroller: {
        marginTop: 10
    },
    dateScroller: {
        marginTop: 10
    },
    monthTouchableDefault: {
        backgroundColor: Colors.COLOR_WHITE,
        padding: 9,
        paddingHorizontal: 20,
        borderColor: Colors.COLOR_BLACK,
        borderWidth: 1,
        borderRadius: 20,
        marginLeft: 5,
        marginRight: 5
    },
    monthTouchableSelected: {
        backgroundColor: Colors.COLOR_SECONDARY,
        padding: 9,
        paddingHorizontal: 20,
        borderColor: Colors.COLOR_SECONDARY,
        borderWidth: 1,
        borderRadius: 20,
        marginLeft: 5,
        marginRight: 5
    },
    dateTouchableDefault: {
        backgroundColor: Colors.COLOR_WHITE,
        padding: 9,
        borderRadius: 20,
        marginLeft: 5,
        marginRight: 5
    },
    dateTouchableSelected: {
        backgroundColor: Colors.COLOR_SECONDARY,
        padding: 5,
        paddingBottom: 0,
        paddingTop: 10,
        paddingHorizontal: 15,
        borderRadius: 1000,
        marginLeft: 5,
        marginRight: 5
    },
    dateTextView: {
        width: 40,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    jobCardContainer: {
        margin: 10,
        padding: 13,
        backgroundColor: Colors.COLOR_WHITE,
        elevation: 5,
        shadowRadius: 5,
        shadowOpacity: 0.2,
        shadowColor: Colors.COLOR_BLACK,
        shadowOffset: {
            width: 0,
            height: 0
        },
        width: '95%',
        borderRadius: 8
    },
    firstLine: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    typeIconBackground: {
        padding: 10,
        marginRight: 10,
        borderRadius: 1000,
        backgroundColor: Colors.COLOR_GRAY_LIGHT
    },
    typeIcon: {
        width: 24,
        height: 24
    },
    stars: {
        width: 24,
        height: 24,
        marginRight: 5
    },
    starsRow: {
        flexDirection: 'row',
        marginTop: 10
    },
    bottomText: {
        marginTop: 10
    },
    emptyContainer: {
        marginTop: 20,
        alignItems: 'center',
    },
})

const mapStateToProps = state => {
    return {
        jobList: state.jobReducer.completedJobsList,
        fetching: state.jobReducer.fetchingCompletedJobs,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        changeScreen: (screen) => dispatch(appActions.changeCurrentScreen(screen))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(index);