import React, { Component } from 'react';
import { NetInfo } from 'react-native';
import BottomNavigator from '../navigation/BottomNavigator'
import { ChooseFromHeader } from '../navigation/NavigationHeader';
import { Colors, Constants } from '../config';
import { connect } from 'react-redux';
import { getLocalizedString } from '../utils/Localizer';
import * as appActions from '../redux/actions/AppActions';

class index extends Component {

    static navigationOptions = ({ navigation }) => {

        const { params = {} } = navigation.state;
        const header = <ChooseFromHeader
            elevation={0}
            backgroundColor={Colors.COLOR_PRIMARY}
            textColor={Colors.COLOR_WHITE}
            title={params.title}
            showSearch={params.showSearch}
            showHome={params.showHome}
            onPressHome={params.onPressHome}
            onPressSearch={params.onPressSearch}
            statusBarHeight={params.statusBarHeight} />;
        return {
            header: header
        };
    };

    constructor() {
        super();
        this.localization = getLocalizedString('en', Constants.TabNavigator);
    }

    componentDidMount() {
        this.props.navigation.setParams({
            showSearch: false,
            showHome: true,
            title: this.localization.MyJobs.toUpperCase(),
            onPressHome: this.onNavBack,
            onPressSearch: this.onPressButtonSearch,
            statusBarHeight: this.props.statusBarHeight,
        });
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);

        // Hearbeat
        this.livePulse() // Initial call
        setInterval(() => this.livePulse(), 60000); // Once a minute

        /** TESTING PURPOSES ONLY */
        // const payload = {
        //     show: true,
        //     type: Constants.alertType.ERROR,
        //     title: '',
        //     message: 'Something went wrong. Please try again later.',
        //     positiveButton: true,
        //     positiveButtonActionType: Constants.alertActionTypes.SOMETHING_WENT_WRONG,
        //     positiveButtonText: 'OK',
        //     negativeButton: false,
        //     negativeButtonText: '',
        //     negativeButtonActionType: '',
        //     additionalPayload: {}
        // };
        // this.props.showAlert(payload);
    }

    livePulse = () => {
        this.props.livepulse();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.screen !== this.props.screen) {
            let title = '';
            switch (this.props.screen) {
                case Constants.screenTypes.MyJobs:
                    title = this.localization.MyJobs.toUpperCase();
                    break;
                case Constants.screenTypes.Completed:
                    title = this.localization.Completed.toUpperCase();
                    break;
                case Constants.screenTypes.Upcoming:
                    title = this.localization.Upcoming.toUpperCase();
                    break;
                case Constants.screenTypes.Profile:
                    title = this.localization.Profile.toUpperCase();
                    break;
            }
            this.props.navigation.setParams({ 'title': title });
            this.props.navigation.setParams({ 'showSearch': this.props.screen === 'MyJobs' ? true : false });

        }
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    onNavBack = () => {
        this.props.screenProps.onPressHome();
    }

    handleConnectivityChange = async () => {
        let hasInternet;
        try {
            const makeACallToGoogle = await fetch('https://www.google.com', {
                headers: {
                    'Cache-Control': 'no-cache, no-store, must-revalidate',
                    Pragma: 'no-cache',
                    Expires: 0,
                },
            });
            hasInternet = makeACallToGoogle.status === 200;
        } catch (e) {
            hasInternet = false;
        }

        this.props.changeAppNetworkStatus(hasInternet);
    }

    render() {
        return (
            < BottomNavigator />
        );
    }
}

const mapStateToProps = state => {
    return {
        screen: state.appReducer.currentPage
    };
};

function mapDispatchToProps(dispatch) {
    return {
        livepulse: () => dispatch(appActions.heartBeat()),
        changeAppNetworkStatus: (payload) => dispatch(appActions.changeAppNetworkStatus(payload)),
        showAlert: (payload) => dispatch(appActions.showAlert(payload)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(index);