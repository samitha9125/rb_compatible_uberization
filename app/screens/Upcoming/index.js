import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, UIManager, findNodeHandle, Image, WebView } from 'react-native';
import PropTypes from 'prop-types';
import MapView, { Marker } from 'react-native-maps';
import Permissions from 'react-native-permissions'
import {
    UserLocationMark,
    GetMyLocationMark,
    LocationMark
} from '../Jobs/Components/Markers';
import { connect } from 'react-redux';
import MapPanel from '../../components/Panels/MapPanel';
import { Colors, Images, Constants } from '../../config';
import { Caption, Label } from '../../components/Typography';
import Button from '../../components/Button';
import { rupeeFormat, centsFormat } from '../../utils/StringUtil';
import * as appActions from '../../redux/actions/AppActions';
import * as jobActions from '../../redux/actions/JobActions';
import CustomAlert from './../../components/CustomAlert';
import CustomMap from '../../config/CustomMap.json';
import SnackBar from '../../components/SnackBar';
import normalize from '../../helpers/normalizeText';

const { width } = Dimensions.get('window');
const uri = 'https://celestial-hospital-249.roast.io';
// const uri = 'http://10.0.2.2:3000';
class index extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            tabBarOnPress({ navigation, defaultHandler }) {
                if (params.onlineStatus === Constants.userStatus.ONLINE && !params.isUserOnAJob) {
                    params.onChangeScreen(Constants.screenTypes.Upcoming);
                    defaultHandler();
                }
            }
        };
    };

    constructor(props) {
        super(props);
        this.locationModalRef = React.createRef();
        this.mapRef = React.createRef();
    }

    state = {
        region: {
            latitude: 6.9204128,
            longitude: 79.8555824,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        userRegion: {
            latitude: 6.9204128,
            longitude: 79.8555824,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        mapKey: 1,
        markerID: 0,
        showModal: false,
        number: 0,
        isMarkerPressed: true,
        /**
         * This tracksViewChanges is for necessary to avoid race conditions. 
         * Otherwise app will crash (An error will occur).
         * mapHeight is height of full map
         */
        tracksViewChanges: false,
        mapHeight: 0,
        modalHeight: 0,

        selectedMarker: {}
    }

    componentDidMount() {
        /**
         * Timeout uses here to re-mount component (Marker component).This makes all markers visible when map loads.
         * After one second of time, key value of Markers will be changed. Thus component will re mounted. Bug fixed.
         * 
         * Second timeout is to re-render the entire mapView to focus real user location without pressing get user location
         * Button. This is also for a bug fix.
         */

        setTimeout(() => this.setState({ mapKey: 200, number: 1 }), 3000);
        setTimeout(() => this.getPermissions(), 4000);

        setTimeout(() => {
            UIManager.measure(findNodeHandle(this.mapRef), (frameX, frameY, frameWidth, frameHeight, pageX, pageY) => {
                this.setState({ mapHeight: frameHeight });
            });
        }, 0);

        this.props.navigation.setParams({
            onChangeScreen: this.onChangeScreen,
            onlineStatus: this.props.onlineStatus,
            isUserOnAJob: this.props.isUserOnAJob
        });

    }

    componentDidUpdate(prevProps) {
        if (prevProps.jobList !== this.props.jobList) {
            setTimeout(() => this.forceUpdate(), 1000);
        }

        if (prevProps.onlineStatus !== this.props.onlineStatus) {
            this.props.navigation.setParams({
                onlineStatus: this.props.onlineStatus
            });
        }

        if (prevProps.isUserOnAJob !== this.props.isUserOnAJob) {
            this.props.navigation.setParams({
                isUserOnAJob: this.props.isUserOnAJob
            });
        }
    }

    componentWillUnmount() {
        this.didFocusEvent.remove();
    }

    onChangeScreen = (screen) => {
        this.props.getUpcomingJobs();
        this.props.changeScreen(screen);
    }

    getPermissions = () => {
        Permissions.check('location').then(response => {
            if (response === 'undetermined') {
                Permissions.request('location').then(response => {
                    if (response === 'authorized')
                        this.getCoordinates();
                });
            }
            else if (response === 'authorized') {
                this.getCoordinates();
            } else {
                // Should tell user to enable locations manually.
            }
        });
    }

    getCoordinates = () => {
        try {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const userRegion = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    };
                    console.log('userRegion : ', userRegion);
                    this.setState({ userRegion, isMarkerPressed: false });
                },
                (error) => {
                    console.log('error : ', error);
                },
                { enableHighAccuracy: false, timeout: 20000 }
            );
        } catch (e) {
            console.log('e.message : ', e.message);
        }
    }

    onPressOfMark = (id) => {

        this.props.getMarkerDetails(id);
        const marker = this.props.jobList.find(marker => marker.woId === id);

        const region = {
            latitude: Number(marker.cxLocation.latitude),
            longitude: Number(marker.cxLocation.longitude),
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        };
        console.log('region : ',region);
        this.setState({ showModal: true, region, isMarkerPressed: true });
    }

    hideModal = () => {
        this.setState({ showModal: false });
    }

    panelHeaderAssets = (marker) => {
        let image = '';
        let Sty = '';
        switch (marker.productCategoryId) {
            case 1://Constants.productCategories.Phone:
                image = Images.icons.ic_phone;
                Sty = styles.typeIconPhone;
                break;
            case 2://Constants.productCategories.TV:
                image = Images.icons.ic_tv;
                Sty = styles.typeIcon;
                break;
            case 3://Constants.productCategories.Router:
                image = Images.icons.ic_router;
                Sty = styles.typeIcon;
                break;
        }
        return (
            <Image style={Sty} source={image} />
        );
    }

    onPressAsign = () => {
        this.setState({ showModal: false }, () => {
            const payload = {
                show: true,
                type: Constants.alertType.WARNING,
                title: '',
                message: 'Do you really want to assign this job to you?',
                positiveButton: true,
                positiveButtonActionType: Constants.alertActionTypes.PROCEED_TO_ASSIGN_JOB,
                positiveButtonText: 'YES',
                negativeButton: true,
                negativeButtonText: 'NO',
                negativeButtonActionType: '',
                additionalPayload: {
                    woId: 1,
                    woStatusCode: 'Assigned'
                }
            };
            this.props.showSheet(payload);
        });
    }

    panelHeader = () => {
        const { marker, loadingMarker } = this.props;
        const content = loadingMarker ?
            <View style={styles.loadingView}>
                <Image style={StyleSheet.flatten(styles.loader)} source={Images.gifs.loader} />
            </View> :
            <View style={styles.main}>
                <View style={styles.firstLine}>
                    <View style={styles.typeIconBackground}>
                        {this.panelHeaderAssets(marker)}
                    </View>
                    <Caption style={StyleSheet.flatten(styles.header)} weight='bold'>
                        {marker.woType.charAt(0).toUpperCase() + marker.woType.slice(1)}
                    </Caption>
                </View>
                <View style={styles.subLines}>
                    <View style={styles.row}>
                        <Image style={StyleSheet.flatten(styles.distanceIcon)} source={Images.icons.ic_distance} />
                        <Caption style={StyleSheet.flatten(styles.text)} weight='light'>Total distance</Caption>
                    </View>
                    <Caption style={StyleSheet.flatten(styles.values)} weight='bold'>{(marker.navigation.distance / 1000).toFixed(1)}
                    <Caption style={StyleSheet.flatten(styles.units)} weight='bold'>Km</Caption></Caption>
                </View>
                <View style={styles.subLines}>
                    <View style={styles.row}>
                        <Image style={StyleSheet.flatten(styles.typeIcon)} source={Images.icons.ic_clock} />
                        <Caption style={StyleSheet.flatten(styles.text)} weight='light'>Estimated travel time</Caption>
                    </View>
                    <Caption style={StyleSheet.flatten(styles.values)} weight='bold'>{marker.navigation.duration}
                    <Caption style={StyleSheet.flatten(styles.units)} weight='bold'> min</Caption></Caption>
                </View>
                <View style={styles.finalSubLine}>
                    <View style={styles.row}>
                        <Image style={StyleSheet.flatten(styles.typeIconArchive)} source={Images.icons.ic_archive} />
                        <View>
                            <Caption style={StyleSheet.flatten(styles.final)} weight='light'>{marker.productType}</Caption>
                            <Label style={StyleSheet.flatten(styles.final)} weight='light'>{marker.paymentType}</Label>
                        </View>
                    </View>
                    <Caption style={StyleSheet.flatten(styles.values)} weight='bold'>
                    <Caption style={StyleSheet.flatten(styles.units)} weight='bold'>Rs </Caption>
                    {rupeeFormat(marker.amount) + centsFormat(marker.amount)}</Caption>
                </View>
                <View style={styles.buttonView}>
                    <Button
                        pressed={this.onPressAsign}
                        style={styles.mainButtonStyle}
                        childrenStyle={styles.mainButtonChildStyle}
                        weight='bold'>Assign</Button>
                </View>
            </View>;
        return (content);
    }

    render() {
        const { userRegion, region, isMarkerPressed, mapKey } = this.state;

        return (
            <View ref={ref => this.mapRef = ref} style={styles.mapContainer}>
                <MapPanel header={this.panelHeader} body={null} show={this.state.showModal} hideModal={this.hideModal} />
                <MapView
                    key={mapKey}
                    style={styles.map}
                    showsMyLocationButton={false}
                    showsUserLocation={false}
                    initialRegion={region}
                    region={isMarkerPressed ? region : userRegion}
                    maxZoomLevel={19}
                    onRegionChangeComplete={() => { }}
                    loadingEnabled={true}
                    zoomEnabled={true}
                    showsScale={false}
                    showsCompass={false}
                    showsBuildings={false}
                    showsTraffic={false}
                    showsIndoors={false}
                    showsPointsOfInterest={false}
                    customMapStyle={CustomMap}>
                    {
                        this.props.jobList.map(marker => {

                            if (this.props.jobList.length > 0) {
                                return (
                                    <Marker
                                        onLoad={() => this.forceUpdate()}
                                        key={marker.woId + '_' + Date.now()}
                                        coordinate={{ longitude: Number(marker.cxLocation.longitude), latitude: Number(marker.cxLocation.latitude) }}
                                        onPress={() => this.onPressOfMark(marker.woId)}
                                    >
                                        <LocationMark markerType={marker.productCategoryId} />
                                    </Marker>
                                );
                            }
                        })
                    }
                    <Marker
                        key={87342892347}
                        coordinate={{
                            longitude: Number(userRegion.longitude),
                            latitude: Number(userRegion.latitude)
                        }}
                        onPress={() => { }}>
                        <UserLocationMark onLoad={() => this.forceUpdate()} />
                    </Marker>
                </MapView>
                <TouchableOpacity onPress={this.getCoordinates} style={styles.currentLocationButton}>
                    <GetMyLocationMark />
                </TouchableOpacity>
                <CustomAlert />
                <SnackBar />
            </View>
        );
    }
}

index.propTypes = {
    userRegion: PropTypes.object,
    getCoordinates: PropTypes.func,
    navigation: PropTypes.object,
    getUpcomingJobs: PropTypes.func,
};

const styles = StyleSheet.create({
    mapContainer: {
        flex: 10,
        width: '100%',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    webView: {
        width: width,
        height: 200,
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1
    },
    currentLocationButton: {
        position: 'absolute',
        left: width * 0.82,
        right: 10,
        bottom: 14,
        zIndex: 0,
    },
    firstLine: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.COLOR_GRAY_LIGHT,
    },
    subLines: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.COLOR_GRAY_LIGHT,
        paddingTop: 20,
        paddingLeft: 10
    },
    finalSubLine: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: Colors.COLOR_GRAY_LIGHT,
        paddingTop: 20,
    },
    typeIconBackground: {
        padding: 10,
        height: 44,
        width: 44,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        borderRadius: 1000,
        backgroundColor: Colors.COLOR_GRAY_LIGHT
    },
    typeIconPhone: {
        width: 14,
        height: 24
    },
    typeIcon: {
        width: 24,
        height: 24
    },
    typeIconArchive: {
        width: 26,
        height: 24,
        marginLeft: 8
    },
    main: {
        width: '100%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        marginLeft: 20,
        color: Colors.COLOR_GRAY_DARK
    },
    header: {
        color: Colors.COLOR_BLACK
    },
    final: {
        marginLeft: 20,
        color: Colors.COLOR_GRAY_DARK
    },
    buttonView: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 20,
        marginTop: 20,
    },
    mainButtonStyle: {
        backgroundColor: Colors.COLOR_SECONDARY,
        width: '48%',
        borderRadius: 4
    },
    mainButtonChildStyle: {
        color: Colors.COLOR_WHITE
    },
    distanceIcon: {
        width: 24,
        height: 26
    },
    loadingView: {
        width: '100%',
        height: '15%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loader: {
        height: 40,
        width: 40,
        marginTop: 40
    },
    values: {
        color: Colors.COLOR_BLACK
    },
    units: {
        color: Colors.COLOR_BLACK,
        fontSize:normalize(13),
    }

});

const mapStateToProps = state => {
    return {
        jobList: state.jobReducer.upcomingJobsList,
        marker: state.jobReducer.selectedMarkerDetails,
        loadingMarker: state.jobReducer.isLoadingMarkerdetails,
        onlineStatus: state.jobReducer.userOnlineStatus,
        isUserOnAJob: state.jobReducer.isUserOnAJob
    };
};

function mapDispatchToProps(dispatch) {
    return {
        changeScreen: (screen) => dispatch(appActions.changeCurrentScreen(screen)),
        getUpcomingJobs: () => dispatch(jobActions.getUpcomingJobsAction()),
        showSheet: (payload) => dispatch(appActions.showAlert(payload)),
        getMarkerDetails: (id) => dispatch(jobActions.getMarkerDetails(id)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(index);