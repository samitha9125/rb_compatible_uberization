
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

export const getMonthsWithYear = (startingDate, numberOfMonths) => { //startingMonth is a JS Date.

    let monthNamesArray = [];
    let startingMonth = startingDate.getMonth();
    let startingYear = startingDate.getFullYear();

    for (let i = 0; i < numberOfMonths; i++) {

        let yearCount = Math.floor((startingMonth + i) / monthNames.length);
        let year = Number(startingYear) + yearCount;
        monthNamesArray.push(monthNames[startingMonth + i - (yearCount) * monthNames.length] + ' ' + year);

    }
    return monthNamesArray;

}

export const getDatesOfAMonth = (formatedMonth) => { // formatedMonth is MMM YYYY. ex: Apr 2019

    let splittedArray = formatedMonth.split(' ');
    let month = monthNames.findIndex((val) => val === splittedArray[0]);

    let date = new Date(splittedArray[1] + '/' + (Number(month) + 1) + '/01');
    let staticDate = new Date(splittedArray[1] + '/' + (Number(month) + 1) + '/01');

    let days = [];
    if (getMonthName(staticDate.getMonth()) + ' ' + staticDate.getFullYear() === getMonthName(new Date().getMonth()) + ' ' + new Date().getFullYear()) {
        console.log('here');
        while (Number(date.getDate()) <= Number(new Date().getDate())) {
            days.push(dayNames[date.getDay()] + ' ' + date.getDate());
            date.setDate(date.getDate() + 1);
        }
    } else {
        while (date.getMonth() === staticDate.getMonth()) {
            days.push(dayNames[date.getDay()] + ' ' + date.getDate());
            date.setDate(date.getDate() + 1);
        }
    }


    return days;
}

export const getDayName = (date) => {
    return dayNames[date];
}

export const getMonthName = (month) => {
    return monthNames[month];
}

export function rupeeFormat(value) {
    let strValue = Number(value).toFixed(2).toString();
    let whole = strValue.split('.')[0];
    strValue = localeString(whole);
    return strValue;
}

export function centsFormat(value) {
    let strValue = Number(value).toFixed(2).toString();
    let cents = strValue.split('.')[1];
    return '.' + cents;
}

export function localeString(x, sep, grp) {
    let sx = ('' + x).split('.'), s = '', i, j;
    sep || (sep = ','); // default seperator
    grp || grp === 0 || (grp = 3); // default grouping
    i = sx[0].length;
    while (i > grp) {
        j = i - grp;
        s = sep + sx[0].slice(j, i) + s;
        i = j;
    }
    s = sx[0].slice(0, i) + s;
    sx[0] = s;
    return sx.join('.');
}