import { Constants } from '../config';
import TabNavigator from '../localization/TabNavigator.json';
import Profile from '../localization/Profile.json';

/**
 * This uses to change the language of the entire app.
 */

export const getLocalizedString = (lang, screen) => {
    switch (lang) {
        case 'en':
            return getValidJSONArray(screen).en;
        case 'si':
            return getValidJSONArray(screen).si;
        case 'ta':
            return getValidJSONArray(screen).ta;
        default:
            break;
    }
};

const getValidJSONArray = (screen) => {
    switch (screen) {
        case Constants.TabNavigator:
            return TabNavigator;
        case Constants.screenTypes.Profile:
            return Profile;
        default:
            break;
    }
};
