import { createStore, applyMiddleware,combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducers from '../redux/reducers/'; 
import sagas from '../sagas';


const sagaMiddleware = createSagaMiddleware();
const reducers = combineReducers(rootReducers);
const store = createStore(
    reducers,
    applyMiddleware(sagaMiddleware)
)

const configureStore = () => {
    return { store };
};

sagaMiddleware.run(sagas);

export default configureStore;
