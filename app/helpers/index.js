
/**
 * Uses to check whether a given object is usable. Emnpty,null or undefined
 */
export const isUnusableObject = (object) => {
    return object === null || object === undefined ||
        Object.entries(object).length === 0 && object.constructor === Object;
}