import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Images, Colors } from '../config';
import { Subtitle } from '../components/Typography';

const ChooseFromHeader = props => {

    const searchComponent = props.showSearch ?
        <TouchableOpacity style={Styles.seachView} onPress={props.onPressSearch}>
            <Image style={[Styles.searchIcon, { tintColor: props.textColor }]} source={Images.icons.ic_search} />
        </TouchableOpacity>
        : null;
    const homeComponent = props.showHome ?
        <TouchableOpacity style={Styles.seachView} onPress={props.onPressHome}>
            <Image style={[Styles.homeIcon, { tintColor: props.textColor }]} source={Images.icons.ic_home} />
        </TouchableOpacity>
        : null;

    return (
        <View style={[
            Styles.container, {
                elevation: props.elevation,
                backgroundColor: props.backgroundColor,
            }]}>
            {homeComponent}
            <View style={Styles.title}>
                <Subtitle weight='normal' style={{ color: props.textColor }}>{props.title}</Subtitle>
            </View>
            {searchComponent}
        </View >
    );
};

export { ChooseFromHeader };


const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    seachView: {

    },
    searchIcon: {
        marginRight: 10,
        height: 28,
        width: 28
    },
    homeIcon: {
        margin: 20,
        marginLeft: 20,
        marginRight: 26,
    }
});
