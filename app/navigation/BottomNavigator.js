import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Jobs from '../screens/Jobs';
import Completed from '../screens/CompletedJobs';
import Upcoming from '../screens/Upcoming';
import Profile from '../screens/Profile';
import TabComponent from '../components/TabNavigator/TabComponent';
import { Constants } from '../config';

const BottomNavigator = createBottomTabNavigator(
    {
        Job: {
            screen: Jobs,
            navigationOptions: {
                tabBarIcon: ({ focused }) => <TabComponent type={Constants.screenTypes.MyJobs} focused={focused} />,
            }
        },
        // Completed: {
        //     screen: Completed,
        //     navigationOptions: {
        //         tabBarIcon: ({ focused }) => <TabComponent type={Constants.screenTypes.Completed} focused={focused} />,
        //     }
        // },
        Upcoming: {
            screen: Upcoming,
            navigationOptions: {
                tabBarIcon: ({ focused }) => <TabComponent type={Constants.screenTypes.Upcoming} focused={focused} />,
            }
        },
        // Profile: {
        //     screen: Profile,
        //     navigationOptions: {
        //         tabBarIcon: ({ focused }) => <TabComponent type={Constants.screenTypes.Profile} focused={focused} />
        //     }
        // },
    },
    {
        tabBarOptions: {
            showLabel: false,
            style: {
                height: 60
            }
        },
        initialRouteName: 'Job',
        lazy: false,
        swipeEnabled: true,
        optimizationsEnabled: true,
    },
);


const AppContainer = BottomNavigator;
export default AppContainer;