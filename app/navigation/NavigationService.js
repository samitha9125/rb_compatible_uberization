import { NavigationActions } from 'react-navigation';

let _navigator;

function setMyJobNavigator(navigatorRef) {
    _navigator = navigatorRef;
}


function Navigate(routeName, params) {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params
        })
    );
}

// add other navigation functions that you need and export them

export default {
    Navigate,
    setMyJobNavigator
};
