import { createStackNavigator } from "react-navigation";
import HomeScreen from '../screens';


const AppNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: { gesturesEnabled: false },
    }
},
    {
        initialRouteName: 'Home',
    }
);

export default AppNavigator;